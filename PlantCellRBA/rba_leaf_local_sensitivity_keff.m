%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% script performing the local sensitibity analysis of the RBA
% model of the plant cell (Figure 6 and Supplementary figure 6)
% Individual variation of enzyme and transporter efficiencies
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Research software: PlantCellRBA
% @ Copyright (c) INRAE
% Author: Anne Goelzer (INRAE), anne.goelzer@inrae.fr
% LICENCE: EUPL v1.2 (https://eupl.eu/1.2/en/)
% Requires: MATLAB and IBM ILOG CPLEX (v12.6 or v12.8) licences for code running
% Creation and last modification: 12 May 2023
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% This line needs to be updated with the path to CPLEX
% addpath My/path/to/Cplex
addpath ./visualisation/
addpath ./utils/
addpath ./RBA_xml/
addpath ./RBA_mat/

clear all;
close all;

disp('---- Init RBA ----')
% load prot, RNA, DNA
load RBA_mat/Prot_RNA_DNA_forRBA
    
% load Pm Process
load RBA_mat/mol_machines
    
% load metabolism: compartments, Reac, Metab S
load RBA_mat/metabolism
    
% load targets:TargetsMF TargetDensities
load RBA_mat/targets
    
%  load Parameters: Parameters ParameterFunctions
load RBA_mat/parameters

%----------------------------------------------------    

% Report functional annotation 
[Reac,Subsystem,Subsystem_curated] = report_functional_annotation_from_FBA(Reac, 'RBA_mat/AraCore.mat');

% Values from Suplice et al. 2014
% Table supp 2 for 18.97mg
PhysioPlante.gDW_gFW = 8.35/100; % fraction of DW in FW
PhysioPlante.protein_content_mg_FW = 18.97; % mg/gFW
PhysioPlante.SLA = 31.6/1000; % specific leaf area. m2/kgDW converted in m2/gDW in Pyl et al. 2012
PhysioPlante.day_length = 8; % hours


%-----------------------------------------------
% Initialisation of the medium from the file medium.tsv in RBA-xml
% directory
Medium = import_medium('e');
Medium.M_CO2_e = 200e-3/1.01325; % 50 to 400 umol
Medium.M_O2_e = 210/1.01325;
Medium.M_hnu_e = 1000; %umol
Medium.M_Suc_e =0;
Temperature_celsius = 21;


% update the structure functions with medium
ParameterFunctions_m = update_function_medium(ParameterFunctions,Medium);


% Update transporter efficiencies with medium
Pm = update_max_efficiency(Pm,Parameters,ParameterFunctions_m);

% Update Rubisco efficiency with medium 
Pm = update_efficiency_rubisco(Pm,Medium.M_CO2_e,Medium.M_O2_e,Medium.M_hnu_e,Temperature_celsius);


% remove the boundary metabolite from the stoichioetry matrix S
S_full = S;
S(id_ex,:) = [];

% on update la structure
MetabFull = Metab;
clear Metab

listeMetabFull = fieldnames(MetabFull);
NMetFull = length(listeMetabFull);
cpt = 1;
for i=1:NMetFull
    % test si machine non nulle
    if MetabFull.(listeMetabFull{i}).boundaryCondition == 0
        Metab.(listeMetabFull{i}) = MetabFull.(listeMetabFull{i});
        Metab.(listeMetabFull{i}).number_S = cpt;
        cpt = cpt+1;
    end
end




%-----------------
% Init struc
Init_RBA.Pm = Pm;
Init_RBA.ProcessFull = ProcessFull;
Init_RBA.ListeAllElementMachine = ListeAllElementMachine;
Init_RBA.Medium = Medium;
Init_RBA.S_full = S_full;
Init_RBA.S = S;
Init_RBA.status_nu = status_nu;
Init_RBA.id_met_boundary = id_ex;
Init_RBA.Comp = Comp;
Init_RBA.Metab = Metab;
Init_RBA.Reac = Reac;
Init_RBA.RNA = RNA;
Init_RBA.DNA = DNA;
Init_RBA.ParameterFunctions_m = ParameterFunctions_m;
Init_RBA.Parameters = Parameters;
Init_RBA.TargetsMF = TargetsMF;
Init_RBA.TargetsDensities = TargetDensities;
Init_RBA.TargetMC = TargetMC;
Init_RBA.TargetMetab = TargetMetab;
Init_RBA.IdTargetReac = IdTargetReac;
Init_RBA.IdAllTargetSpecies = IdAllSpecies;
Init_RBA.ProcessingMaps = ProcessingMaps;
Init_RBA.Subsystem = Subsystem;
Init_RBA.Subsystem_curated = Subsystem_curated;
Init_RBA.Prot = Prot;
Init_RBA.Temperature_celsius = Temperature_celsius;


optionsRBA.seuil_mu = 1e-4;
optionsRBA.display_cplex = 'off';
optionsRBA.display_iter_mu = 'off';


disp('---- Build model (takes ~1min) ----')
[RBAmodel] = build_RBAmodel_eukaryotes(Init_RBA);

% list of parameter/enzyme to vary
listePm = fieldnames(Pm);
factor_mul = [0.5 2];
mu_var = [];
X_var = cell(2,1);
Pm_original = Pm;
RBAmodel_original = RBAmodel;
RBAmodel_tmp = RBAmodel_original;
name_param_enz = {};
Nm = length(listePm);
for k=1:length(factor_mul)
    cpt = 1;
    
    for i=1:length(listePm)
        disp(['Factor ' num2str(factor_mul(k)) ', Param testé: ' listePm{i}])
        if strcmpi(listePm{i},'R_RBC_h_enzyme') || strcmpi(listePm{i},'R_RBC_h_duplicate_2_enzyme') ...
                || strcmpi(listePm{i},'R_RBC_h_duplicate_3_enzyme') || strcmpi(listePm{i},'R_RBC_h_duplicate_4_enzyme') ...
                || strcmpi(listePm{i},'R_RBO_h_enzyme') || strcmpi(listePm{i},'R_RBO_h_duplicate_2_enzyme') ...
                || strcmpi(listePm{i},'R_RBO_h_duplicate_3_enzyme') || strcmpi(listePm{i},'R_RBO_h_duplicate_4_enzyme') ...
                || strcmpi(listePm{i},'R_PSII_h_enzyme') || strcmpi(listePm{i},'R_PSII2_h_enzyme') ...
                || strcmpi(listePm{i},'R_PSII3_h_enzyme') || strcmpi(listePm{i},'R_PSII4_h_enzyme') || contains(listePm{i},'duplicate','IgnoreCase',1) ...
                || strcmpi(listePm{i},'R_PSI2_h_enzyme') || strcmpi(listePm{i},'R_PSI3_h_enzyme') || strcmpi(listePm{i},'R_PSI4_h_enzyme') ...
                || strcmpi(listePm{i},'R_PSI5_h_enzyme') || strcmpi(listePm{i},'R_PSI6_h_enzyme') || strcmpi(listePm{i},'R_PSI7_h_enzyme') ...
                || strcmpi(listePm{i},'R_PSI8_h_enzyme')
            
            disp(['Param non traite: ' listePm{i}])
            
        else
            
            if strcmpi(listePm{i},'R_PSI1_h_enzyme')
                Pm.R_PSI1_h_enzyme.keff_f = Pm_original.R_PSI1_h_enzyme.keff_f*factor_mul(k);
                Pm.R_PSI1_h_enzyme.keff_b = Pm_original.R_PSI1_h_enzyme.keff_b*factor_mul(k);
                Pm.R_PSI2_h_enzyme.keff_f = Pm_original.R_PSI2_h_enzyme.keff_f*factor_mul(k);
                Pm.R_PSI2_h_enzyme.keff_b = Pm_original.R_PSI2_h_enzyme.keff_b*factor_mul(k);
                Pm.R_PSI3_h_enzyme.keff_f = Pm_original.R_PSI3_h_enzyme.keff_f*factor_mul(k);
                Pm.R_PSI3_h_enzyme.keff_b = Pm_original.R_PSI3_h_enzyme.keff_b*factor_mul(k);
                Pm.R_PSI4_h_enzyme.keff_f = Pm_original.R_PSI4_h_enzyme.keff_f*factor_mul(k);
                Pm.R_PSI4_h_enzyme.keff_b = Pm_original.R_PSI4_h_enzyme.keff_b*factor_mul(k);
                Pm.R_PSI5_h_enzyme.keff_f = Pm_original.R_PSI5_h_enzyme.keff_f*factor_mul(k);
                Pm.R_PSI5_h_enzyme.keff_b = Pm_original.R_PSI5_h_enzyme.keff_b*factor_mul(k);
                Pm.R_PSI6_h_enzyme.keff_f = Pm_original.R_PSI6_h_enzyme.keff_f*factor_mul(k);
                Pm.R_PSI6_h_enzyme.keff_b = Pm_original.R_PSI6_h_enzyme.keff_b*factor_mul(k);
                Pm.R_PSI7_h_enzyme.keff_f = Pm_original.R_PSI7_h_enzyme.keff_f*factor_mul(k);
                Pm.R_PSI7_h_enzyme.keff_b = Pm_original.R_PSI7_h_enzyme.keff_b*factor_mul(k);
                Pm.R_PSI8_h_enzyme.keff_f = Pm_original.R_PSI8_h_enzyme.keff_f*factor_mul(k);
                Pm.R_PSI8_h_enzyme.keff_b = Pm_original.R_PSI8_h_enzyme.keff_b*factor_mul(k);
                
                % PSI1
                for ppp=0:7
                    RBAmodel_tmp.matrix.A0(RBAmodel_tmp.Sind.ind_iq_nu+i+ppp,RBAmodel_tmp.Sind.offset_concentration+i+ppp) = -Pm.(listePm{i+ppp}).keff_f;
                    RBAmodel_tmp.matrix.A0(RBAmodel_tmp.Sind.ind_iq_nu+Nm+i+ppp,RBAmodel_tmp.Sind.offset_concentration+i+ppp) = -Pm.(listePm{i+ppp}).keff_b;
                    RBAmodel_tmp.matrix.A0_mean_complex(RBAmodel_tmp.Sind.ind_iq_nu+i+ppp,RBAmodel_tmp.Sind.offset_concentration+i+ppp) = -Pm.(listePm{i+ppp}).keff_f;
                    RBAmodel_tmp.matrix.A0_mean_complex(RBAmodel_tmp.Sind.ind_iq_nu+Nm+i+ppp,RBAmodel_tmp.Sind.offset_concentration+i+ppp) = -Pm.(listePm{i+ppp}).keff_b;
                end
            else
                Pm.(listePm{i}).keff_f = Pm_original.(listePm{i}).keff_f*factor_mul(k);
                Pm.(listePm{i}).keff_b = Pm_original.(listePm{i}).keff_b*factor_mul(k);
                %Init_RBA.Pm = Pm;
                
                % et maintenant on update uniquement la matrice A0
                RBAmodel_tmp.matrix.A0(RBAmodel_tmp.Sind.ind_iq_nu+i,RBAmodel_tmp.Sind.offset_concentration+i) = -Pm.(listePm{i}).keff_f;
                RBAmodel_tmp.matrix.A0(RBAmodel_tmp.Sind.ind_iq_nu+Nm+i,RBAmodel_tmp.Sind.offset_concentration+i) = -Pm.(listePm{i}).keff_b;
                RBAmodel_tmp.matrix.A0_mean_complex(RBAmodel_tmp.Sind.ind_iq_nu+i,RBAmodel_tmp.Sind.offset_concentration+i) = -Pm.(listePm{i}).keff_f;
                RBAmodel_tmp.matrix.A0_mean_complex(RBAmodel_tmp.Sind.ind_iq_nu+Nm+i,RBAmodel_tmp.Sind.offset_concentration+i) = -Pm.(listePm{i}).keff_b;
            
            end
            
            Init_RBA.Pm = Pm;
            

            
            disp('---- Look for µ  ----')
            [RBAopt] = solve_RBAmodel_eukaryotes(RBAmodel_tmp,Init_RBA,optionsRBA);
            
            if ~isempty(RBAopt)
                mu_var(k,cpt) = RBAopt.mu_opt;
                X_var{k}(:,cpt) = RBAopt.X2;
            end
            
            % on reinitialise ParameterFunctions_m et Pm à sa valeur
            % d'origine: ParameterFunctions_m_original
            Pm = Pm_original;
            RBAmodel_tmp = RBAmodel_original;
            
            % name param
            name_param_enz{k}{cpt} = listePm{i};
            %
            cpt = cpt + 1;
            
        end
        
    end

end



%save simulation/Sensitivity/Sensitivity_enzyme_individual_parameter mu_var X_var factor_mul Init_RBA



