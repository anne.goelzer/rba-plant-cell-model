function [ReacRBA,Subsystem1, Subsystem2] = report_functional_annotation_from_FBA(ReacRBA, name_mat_fba)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Research software: PlantCellRBA
% @ Copyright (c) INRAE
% Author: Anne Goelzer (INRAE), anne.goelzer@inrae.fr
% LICENCE: EUPL v1.2 (https://eupl.eu/1.2/en/)
% Requires: MATLAB and IBM ILOG CPLEX (v12.6 or v12.8) licences for code running
% Creation and last modification: 12 May 2023
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
eval(['load ' name_mat_fba])
ReacFBA = Reac;

listeR_RBA = fieldnames(ReacRBA); n1 = length(listeR_RBA);
listeR_FBA = fieldnames(ReacFBA); n2 = length(listeR_FBA);

for i=1:n1
    ii = strfind(listeR_RBA{i},'_duplicate');
    if ~isempty(ii)
       idR = listeR_RBA{i}(1:(ii-1));
    else
        idR = listeR_RBA{i};
    end
    pp = find(contains(listeR_FBA,idR,'IgnoreCase',1)>0);
    
    if pp>0
        if isfield( ReacFBA.(listeR_FBA{pp(1)}),'subsystem')
            ReacRBA.(listeR_RBA{i}).subsystem = ReacFBA.(listeR_FBA{pp(1)}).subsystem;
        else
            ReacRBA.(listeR_RBA{i}).subsystem = [];
        end
        if isfield(ReacFBA.(listeR_FBA{pp(1)}),'subsystem_curated')
            ReacRBA.(listeR_RBA{i}).subsystem_curated = ReacFBA.(listeR_FBA{pp(1)}).subsystem_curated;
        else
            ReacRBA.(listeR_RBA{i}).subsystem_curated = [];
        end
    else
        %idR
    end
end
ReacRBA.R_maintenance_atp.subsystem_curated = 'maintenance';

Subsystem1 = Subsystem;
Subsystem2 = Subsystem_curated;
