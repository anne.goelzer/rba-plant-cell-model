function DataOpt2 = elimine_u(DataOpt)   
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Research software: PlantCellRBA
% @ Copyright (c) INRAE
% Author: Anne Goelzer (INRAE), anne.goelzer@inrae.fr
% LICENCE: EUPL v1.2 (https://eupl.eu/1.2/en/)
% Requires: MATLAB and IBM ILOG CPLEX (v12.6 or v12.8) licences for code running
% Creation and last modification: 12 May 2023
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ind_eq_X = DataOpt.ind_eq_X;
Nx = DataOpt.Nx;
Ny = DataOpt.Ny;
Nm = DataOpt.Nm;
Npro = DataOpt.Npro;
Ntg = DataOpt.Ntg;
Nc = DataOpt.Nc;
Nlig_add = DataOpt.Nlig_add;
offset_concentration = DataOpt.offset_concentration;
offset_flux = DataOpt.offset_flux;
ind_duplicate = DataOpt.ind_duplicate;

ind_eq_Tfix = DataOpt.ind_eq_Tfix;
ind_iq_D = DataOpt.ind_iq_D;
ind_iq_Pro = DataOpt.ind_iq_Pro;
ind_eq_Y = DataOpt.ind_eq_Y;
ind_uT = DataOpt.ind_uT;
% matrices
Aeq = DataOpt.Aeq;
beq = DataOpt.beq;
A = DataOpt.A;
b =  DataOpt.b;
LB =  DataOpt.LB;
UB =  DataOpt.UB;
f =  DataOpt.f;
Pm =  DataOpt.Pm;
status_nu = DataOpt.status_nu;
name_Xv = DataOpt.name_Xv;
name_Aeq = DataOpt.name_Aeq;
name_A = DataOpt.name_A;
name_Xv2 = name_Xv;
name_Aeq2 = name_Aeq;
name_A2 = name_A;

% Eliminate variables lambdaY
C_uYS = Aeq(ind_eq_X+[1:Nx],1:Ny);
C_YS =  Aeq(ind_eq_X+[1:Nx],offset_concentration + [1:Ny]);
C_uYY = A(ind_iq_Pro + [1:Npro],1:Ny);
C_YY = A(ind_iq_Pro + [1:Npro],offset_concentration + [1:Ny]);
uY = -Aeq(ind_eq_Y + [1:Ny], offset_concentration + [1:Ny]);

% Eliminate variables lambda T
C_uTS = Aeq(ind_eq_X+[1:Nx],Ny + [1:Ntg]);
C_TS =  Aeq(ind_eq_X+[1:Nx],offset_concentration + Ny + [1:Ntg]);
C_uTY = A(ind_iq_Pro + [1:Npro],Ny + [1:Ntg]);
C_TY = A(ind_iq_Pro + [1:Npro],offset_concentration + Ny + [1:Ntg]);
uT =  -Aeq(ind_uT, offset_concentration + Ny + [1:Ntg]);
% Eliminate de T = Tfix
bT = beq(ind_eq_Tfix+[1:Ntg]);
C_TD = A(ind_iq_D+[1:Nc],offset_concentration + Ny + [1:Ntg]);

% indices of variables to be removed
% lambdaY lambdaT Y_duplicate T nu_duplicate
%ind_remove_variable = [1:Ny];
mm = max(strcmp('Y_R_RBO_h_enzyme',name_Xv).*[1:length(name_Xv)]);
% Remove RBO enzyme
%nu RBO = keff_oxygenase Erubisco carboxylase
ind_remove_variable = [1:Ny+Ntg mm offset_concentration+ind_duplicate  offset_concentration+Ny+[1:Ntg] offset_flux+ind_duplicate];
name_Xv2(ind_remove_variable) = [];
status_enz_irrev = status_nu;
status_enz_irrev([ind_duplicate mm-offset_concentration]) = [];


clear mm

%-----------------------------------------
% elimination of variables
%-------------------------------------------
%Equalities
Aeq2 = Aeq;
beq2 = beq;
%lY
Aeq2(ind_eq_X+[1:Nx],offset_concentration + [1:Ny]) = C_uYS*uY +C_YS;
%lT+T
beq2(ind_eq_X+[1:Nx]) = beq2(ind_eq_X+[1:Nx]) - (C_uTS*uT +C_TS)*bT;

Aeq2(:,ind_remove_variable) = [];
% remove equality constraints
Aeq2(ind_eq_Y + [1:Ny+Nlig_add+Ntg],:) = [];
beq2(ind_eq_Y + [1:Ny+Nlig_add+Ntg]) = [];

% update name
name_Aeq2(ind_eq_Y + [1:Ny+Nlig_add+Ntg]) = [];

% Inequalities
A2 = A;
b2 = b;
% on update A2 sur les processus
A2(ind_iq_Pro + [1:Npro],offset_concentration + [1:Ny]) =  C_uYY*uY + C_YY;
b2(ind_iq_Pro + [1:Npro]) = b2(ind_iq_Pro + [1:Npro])-(C_uTY*uT + C_TY)*bT;
b2(ind_iq_D + [1:Nc]) = b2(ind_iq_D + [1:Nc])-C_TD*bT;
A2(:,ind_remove_variable) = [];

%A2(ind_iq_Pro + [1:Npro],offset_concentration + [1:Ny]) =  C_uYY*uY + C_YY;
%A2(:,ind_remove_variable) = [];
mm1 = max(strcmp('Capacity_forward_R_RBO_h_enzyme',name_A2).*[1:length(name_A2)]);
mm2 = max(strcmp('Capacity_backward_R_RBO_h_enzyme',name_A2).*[1:length(name_A2)]);
A2([Nc+Npro+ind_duplicate  Nc+Npro+Nm+ind_duplicate  mm1 mm2],:) = [];
b2([Nc+Npro+ind_duplicate  Nc+Npro+Nm+ind_duplicate  mm1 mm2])= [];
name_A2([Nc+Npro+ind_duplicate  Nc+Npro+Nm+ind_duplicate mm1 mm2])= [];


LB2 = LB; LB2(ind_remove_variable) = [];
UB2 = UB; UB2(ind_remove_variable) = [];
f2 = f; f2(ind_remove_variable) = [];


% ajout de la contrainte rubisco
tmpLL = zeros(1,length(name_Xv2));
cc = max(strcmp('nu_R_RBO_h',name_Xv2).*[1:length(name_Xv2)]);
mm = max(strcmp('Y_R_RBC_h_enzyme',name_Xv2).*[1:length(name_Xv2)]);
tmpLL(cc) = 1;
tmpLL(mm) = - Pm.R_RBO_h_enzyme.keff_f;
Aeq2 = [Aeq2; tmpLL];
beq2 = [beq2; 0];
name_Aeq2= [name_Aeq2, 'Rubisco_oxygenase'];


%build output
DataOpt2.A = A2;
DataOpt2.b = b2;
DataOpt2.Aeq = Aeq2;
DataOpt2.beq = beq2;
DataOpt2.f = f2;
DataOpt2.LB = LB2;
DataOpt2.UB = UB2;
DataOpt2.name_A = name_A2;
DataOpt2.name_Aeq = name_Aeq2;
DataOpt2.name_Xv = name_Xv2;
DataOpt2.status_enz_irrev = status_enz_irrev;
