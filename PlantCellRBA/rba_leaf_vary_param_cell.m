%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% script performing the local sensitibity analysis of the RBA
% model of the plant cell (Figure 5 and Supplementary figure 5)
% Variation of Cell physiology parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Research software: PlantCellRBA
% @ Copyright (c) INRAE
% Author: Anne Goelzer (INRAE), anne.goelzer@inrae.fr
% LICENCE: EUPL v1.2 (https://eupl.eu/1.2/en/)
% Requires: MATLAB and IBM ILOG CPLEX (v12.6 or v12.8) licences for code running
% Creation and last modification: 12 May 2023
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% This line needs to be updated with the path to CPLEX
% addpath My/path/to/Cplex

addpath ./visualisation/
addpath ./utils/
addpath ./RBA_xml/
addpath ./RBA_mat/
clear all;
close all;

disp('---- Init RBA ----')
% load prot, RNA, DNA
load RBA_mat/Prot_RNA_DNA_forRBA
    
% load Pm Process
load RBA_mat/mol_machines
    
% load metabolism: compartments, Reac, Metab S
load RBA_mat/metabolism
    
% load targets:TargetsMF TargetDensities
load RBA_mat/targets
    
%  load Parameters: Parameters ParameterFunctions
load RBA_mat/parameters

%----------------------------------------------------    

% Report functional annotation 
[Reac,Subsystem,Subsystem_curated] = report_functional_annotation_from_FBA(Reac, 'RBA_mat/AraCore.mat');

% Values from Suplice et al. 2014
% Table supp 2 for 18.97mg
PhysioPlante.gDW_gFW = 8.35/100; % fraction of DW in FW
PhysioPlante.protein_content_mg_FW = 18.97; % mg/gFW
PhysioPlante.SLA = 31.6/1000; % specific leaf area. m2/kgDW converted in m2/gDW in Pyl et al. 2012
PhysioPlante.day_length = 8; % hours


%-----------------------------------------------
% Initialisation of the medium from the file medium.tsv in RBA-xml
% directory
Medium = import_medium('e');
Medium.M_CO2_e = 200e-3/1.01325; % 50 to 400 umol
Medium.M_O2_e = 210/1.01325;
Medium.M_hnu_e = 1000; %umol
Medium.M_Suc_e =0;
Temperature_celsius = 21;


% update the structure functions with medium
ParameterFunctions_m = update_function_medium(ParameterFunctions,Medium);


% Update transporter efficiencies with medium
Pm = update_max_efficiency(Pm,Parameters,ParameterFunctions_m);


% Update Rubisco efficiency with medium 
Pm = update_efficiency_rubisco(Pm,Medium.M_CO2_e,Medium.M_O2_e,Medium.M_hnu_e,Temperature_celsius);


% remove extracellular metabolites
S_full = S;
S(id_ex,:) = [];

% on update la structure
MetabFull = Metab;
clear Metab

listeMetabFull = fieldnames(MetabFull);
NMetFull = length(listeMetabFull);
cpt = 1;
for i=1:NMetFull
    % test si machine non nulle
    if MetabFull.(listeMetabFull{i}).boundaryCondition == 0
        Metab.(listeMetabFull{i}) = MetabFull.(listeMetabFull{i});
        Metab.(listeMetabFull{i}).number_S = cpt;
        cpt = cpt+1;
    end
end




%-----------------
% Init struc
Init_RBA.Pm = Pm;
Init_RBA.ProcessFull = ProcessFull;
Init_RBA.ListeAllElementMachine = ListeAllElementMachine;
Init_RBA.Medium = Medium;
Init_RBA.S_full = S_full;
Init_RBA.S = S;
Init_RBA.status_nu = status_nu;
Init_RBA.id_met_boundary = id_ex;
Init_RBA.Comp = Comp;
Init_RBA.Metab = Metab;
Init_RBA.Reac = Reac;
Init_RBA.RNA = RNA;
Init_RBA.DNA = DNA;
Init_RBA.ParameterFunctions_m = ParameterFunctions_m;
Init_RBA.Parameters = Parameters;
Init_RBA.TargetsMF = TargetsMF;
Init_RBA.TargetsDensities = TargetDensities;
Init_RBA.TargetMC = TargetMC;
Init_RBA.TargetMetab = TargetMetab;
Init_RBA.IdTargetReac = IdTargetReac;
Init_RBA.IdAllTargetSpecies = IdAllSpecies;
Init_RBA.ProcessingMaps = ProcessingMaps;
Init_RBA.Subsystem = Subsystem;
Init_RBA.Subsystem_curated = Subsystem_curated;
Init_RBA.Prot = Prot;
Init_RBA.Temperature_celsius = Temperature_celsius;


optionsRBA.seuil_mu = 1e-4;
optionsRBA.display_cplex = 'off';
optionsRBA.display_iter_mu = 'off';



% list of parameter to change
listeParam = fieldnames(ParameterFunctions_m);
factor_mul = [0.5 2];
mu_var = [];
X_var = cell(2,1);
ParameterFunctions_m_original = ParameterFunctions_m;
Pm_original = Pm;
listePm = fieldnames(Pm); Nm = length(Pm);

disp('---- Build model (takes ~1 min) ----')
[RBAmodel] = build_RBAmodel_eukaryotes(Init_RBA);
RBAmodel_original =  RBAmodel;
RBAmodel_tmp = RBAmodel;
name_param = {};
cpt = 1;
for k=1:length(factor_mul)
    cpt = 1;
    for i=1:length(listeParam)
        
        disp(['Factor ' num2str(factor_mul(k)) ', Param testé: ' listeParam{i}])
        if strcmpi(listeParam{i},'ribosome_efficiency_MM') || strcmpi(listeParam{i},'chaperone_efficiency_LM') ...
                || startsWith(listeParam{i},'R_Ex_','IgnoreCase',1) || strcmpi(listeParam{i},'zero') || strcmpi(listeParam{i},'inverse_average_protein_length') ...
                           || strcmpi(listeParam{i},'fraction_active_ribosomes') || strcmpi(listeParam{i},'fraction_active_ribosomes')
            
                       disp(['Param non traite: ' listeParam{i}])
        else
            ll = fieldnames(ParameterFunctions_m.( listeParam{i}).listOfParameters);
            for j=1:length(ll)
                if  strcmpi(listeParam{i},'M_M_DASH_ACP_h_concentration') || strcmpi(listeParam{i},'M_ACP_h_concentration')
                    % ici on doit augmenter de 2 met de façon coordonnes.
                    % On est ds la boucle, car pour ces 2 met, les param
                    % sont constants ... donc ll est de dim 1.
                    ParameterFunctions_m.M_M_DASH_ACP_h_concentration.listOfParameters.CONSTANT = factor_mul(k)*ParameterFunctions_m_original.M_M_DASH_ACP_h_concentration.listOfParameters.CONSTANT;
                    ParameterFunctions_m.M_ACP_h_concentration.listOfParameters.CONSTANT = factor_mul(k)*ParameterFunctions_m_original.M_ACP_h_concentration.listOfParameters.CONSTANT;
                    Pm = Pm_original;
                    
                elseif strcmpi(listeParam{i},'default_efficiency') || strcmpi(listeParam{i},'default_transporter_efficiency')
                    ParameterFunctions_m.(listeParam{i}).listOfParameters.(ll{j}) = factor_mul(k)*ParameterFunctions_m_original.(listeParam{i}).listOfParameters.(ll{j});
                    % Parameters ParameterFunctions
                    Pm = update_max_efficiency(Pm_original,Parameters,ParameterFunctions_m);
                    % on update les parametres de la rubisco avec le milieu exterieur
                    Pm = update_efficiency_rubisco(Pm,Medium.M_CO2_e,Medium.M_O2_e,Medium.M_hnu_e,Temperature_celsius);
                    

                    Init_RBA2 = Init_RBA;
                    Init_RBA2.ParameterFunctions_m = ParameterFunctions_m;
                    Init_RBA2.Pm = Pm;
                    [RBAmodel_tmp] = build_RBAmodel_eukaryotes(Init_RBA2);
                    
                else
                    
                    ParameterFunctions_m.(listeParam{i}).listOfParameters.(ll{j}) = factor_mul(k)*ParameterFunctions_m_original.(listeParam{i}).listOfParameters.(ll{j});
                    Pm = Pm_original;
                end
                Init_RBA.ParameterFunctions_m = ParameterFunctions_m;
                Init_RBA.Pm = Pm;
                RBAmodel_tmp.Pm = Pm;
                
                
                disp('---- Look for µ  ----')
                [RBAopt] = solve_RBAmodel_eukaryotes(RBAmodel_tmp,Init_RBA,optionsRBA);
                
                if ~isempty(RBAopt)
                    mu_var(k,cpt) = RBAopt.mu_opt;
                    X_var{k}(:,cpt) = RBAopt.X2;
                end
                
                % on reinitialise ParameterFunctions_m et Pm à sa valeur
                % d'origine: ParameterFunctions_m_original
                ParameterFunctions_m = ParameterFunctions_m_original;
                Pm = Pm_original;
                RBAmodel_tmp = RBAmodel_original;
                
                % name param
                name_param{k}{cpt} = [listeParam{i} '_' ll{j}];
                %
                cpt = cpt + 1;
                
            end
        end
        
    end
end


% save simulation/Sensitivity/Sensitivity_cell_parameter  name_param mu_var X_var factor_mul Init_RBA


