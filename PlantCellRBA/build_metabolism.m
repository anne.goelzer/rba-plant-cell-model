function [Comp, Metab, Reac, S,status_nu,id_ex] = build_metabolism()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Research software: PlantCellRBA
% @ Copyright (c) INRAE
% Author: Anne Goelzer (INRAE), anne.goelzer@inrae.fr
% LICENCE: EUPL v1.2 (https://eupl.eu/1.2/en/)
% Requires: MATLAB and IBM ILOG CPLEX (v12.6 or v12.8) licences for code running
% Creation and last modification: 12 May 2023
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
SMetabolism = xml2struct('RBA_xml/metabolism.xml');

nC = length(SMetabolism.RBAMetabolism.listOfCompartments.compartment);
for i=1:nC
    idC = SMetabolism.RBAMetabolism.listOfCompartments.compartment{i}.Attributes.id;
     Comp.(idC).name = SMetabolism.RBAMetabolism.listOfCompartments.compartment{i}.Attributes.id;
     Comp.(idC).volume = -1;
     Comp.(idC).density_aa = -1;
end
Comp = orderfields(Comp);

% Metabolite
nM = length(SMetabolism.RBAMetabolism.listOfSpecies.species);
id_ex = [];
for i=1:nM
    idM = SMetabolism.RBAMetabolism.listOfSpecies.species{i}.Attributes.id;
    Metab.(idM).name = idM;
    if strcmp(SMetabolism.RBAMetabolism.listOfSpecies.species{i}.Attributes.boundaryCondition,'true')
        Metab.(idM).boundaryCondition = true;
        id_ex = [id_ex i];
    else
        Metab.(idM).boundaryCondition = false;
    end
    Metab.(idM).number = i;
    
    % Mapping localisation
    switch idM(end)
        case 'c'
             Metab.(idM).localisation = 'Cytoplasm';
        case 'm'
             Metab.(idM).localisation = 'Mitochondrion_matrix'; 
        case 'n'
             Metab.(idM).localisation = 'Nucleus';  
        case 'h'
             Metab.(idM).localisation = 'Plastid';              
        case 'p'
             Metab.(idM).localisation = 'Peroxisome';   
        case 'l'
             Metab.(idM).localisation = 'Thylakoid_lumen'; 
        case 'i'
             Metab.(idM).localisation = 'Mitochondrion_interspace';            
        case 'e'
             Metab.(idM).localisation = 'Secreted';
             
            
        otherwise
            disp('unknown metab localisation ')
            idM
            pause
    
    end
end


nR = length(SMetabolism.RBAMetabolism.listOfReactions.reaction);
for i=1:nR
    i
  
    idR = SMetabolism.RBAMetabolism.listOfReactions.reaction{i}.Attributes.id;
    Reac.(idR).name = idR;
    
    
    % reversibility
    if strcmp(SMetabolism.RBAMetabolism.listOfReactions.reaction{i}.Attributes.reversible,'true')
        Reac.(idR).reversible = true;
    elseif strcmp(SMetabolism.RBAMetabolism.listOfReactions.reaction{i}.Attributes.reversible,'false')
        Reac.(idR).reversible = false;
    else
        disp('Type of reaction unknown')
    end
    
    % Reactants
    TmpR = [];
    if isfield(SMetabolism.RBAMetabolism.listOfReactions.reaction{i}.listOfReactants,'speciesReference')   
        nRR = length(SMetabolism.RBAMetabolism.listOfReactions.reaction{i}.listOfReactants.speciesReference);
        if nRR == 1
            TmpR{1} = SMetabolism.RBAMetabolism.listOfReactions.reaction{i}.listOfReactants.speciesReference.Attributes;
        else
            for j=1:nRR
                TmpR{j} = SMetabolism.RBAMetabolism.listOfReactions.reaction{i}.listOfReactants.speciesReference{j}.Attributes;
            end
        end
    end
    Reac.(idR).reactant = TmpR;
    
    TmpR = [];
    if isfield(SMetabolism.RBAMetabolism.listOfReactions.reaction{i}.listOfProducts,'speciesReference')
        nPP = length(SMetabolism.RBAMetabolism.listOfReactions.reaction{i}.listOfProducts.speciesReference);
        if nPP == 1
            TmpR{1} = SMetabolism.RBAMetabolism.listOfReactions.reaction{i}.listOfProducts.speciesReference.Attributes;
        else
            for j=1:nPP
                TmpR{j} = SMetabolism.RBAMetabolism.listOfReactions.reaction{i}.listOfProducts.speciesReference{j}.Attributes;
            end
        end
    end
    Reac.(idR).product = TmpR;
    

    
end

liste_Reac = fieldnames(Reac);
nR = length(liste_Reac);

%------------------------
% Stoichiometry matrixes
%------------------------
S = zeros(nM, nR);
status_nu = zeros(nR,1);

% Build matrix 
for i=1:nR
    idR = liste_Reac{i};
    Reac.(idR).number = i;
    str = [];
    cpt = 1;
    % Parse reactants
    n_subs = length(Reac.(idR).reactant);
    for j=1:n_subs
        S(Metab.(Reac.(idR).reactant{j}.species).number ,i) = - str2num(Reac.(idR).reactant{j}.stoichiometry);
        liste_comp_metab{cpt} = Metab.(Reac.(idR).reactant{j}.species).localisation;
        cpt = cpt+1;
        str = [str Reac.(idR).reactant{j}.stoichiometry ' ' Reac.(idR).reactant{j}.species ' + '];
    end

    % Remove the 2 last characters
    if ~isempty(str)
        if Reac.(idR).reversible
            str(end-1:end) = '<>';
        else
            str(end-1:end) = '->';
        end
    else
        str = '->';
    end
    % Parse  products
    n_prod= length(Reac.(idR).product);
    for j=1:n_prod
        S(Metab.(Reac.(idR).product{j}.species).number ,i) =  str2num(Reac.(idR).product{j}.stoichiometry);
        liste_comp_metab{cpt} = Metab.(Reac.(idR).product{j}.species).localisation;
        cpt = cpt+1;
         str = [str Reac.(idR).product{j}.stoichiometry ' ' Reac.(idR).product{j}.species ' + '];
    end
    str = str(1:end-2);
    Reac.(idR).reaction = str;
    Reac.(idR).number_S = i;
    
    % Reversible/irreversible
    status_nu(i) = Reac.(idR).reversible;
      
    % remove duplicated compartment + sort by alphabetic order 
    liste_comp_metab2 = unique(liste_comp_metab);

    for j=1:length(liste_comp_metab2)
        liste_comp_name_metab{j} = Comp.(liste_comp_metab2{j}).name;
    end
    Reac.(idR).comp = liste_comp_metab2;
    if length(liste_comp_name_metab) == 2
        if sum(cellfun(@strcmp,liste_comp_metab2,{'Cytoplasm','Secreted'})) == 2 || sum(cellfun(@strcmp,liste_comp_metab2,{'Secreted','Cytoplasm'})) == 2
            Reac.(idR).localisation = 'Cell_membrane';
        end       
        if sum(cellfun(@strcmp,liste_comp_metab2,{'Cytoplasm','Plastid'})) == 2
            Reac.(idR).localisation = 'Plastid_membrane';
        end
        if sum(cellfun(@strcmp,liste_comp_metab2,{'Plastid','Secreted'})) == 2
            Reac.(idR).localisation = 'Plastid_membrane';
        end
        if sum(cellfun(@strcmp,liste_comp_metab2,{'Cytoplasm','Peroxisome'})) == 2
            Reac.(idR).localisation = 'Peroxisome_membrane';
        end
        if sum(cellfun(@strcmp,liste_comp_metab2,{'Plastid','Thylakoid_lumen'})) == 2
            Reac.(idR).localisation = 'Thylakoid_membrane';
        end
        if sum(cellfun(@strcmp,liste_comp_metab2,{'Cytoplasm','Nucleus'})) == 2
            Reac.(idR).localisation = 'Nucleus_membrane';
        end
        if sum(cellfun(@strcmp,liste_comp_metab2,{'Cytoplasm','Mitochondrion_matrix'})) == 2
            Reac.(idR).localisation = 'Mitochondrion_inner_membrane';
        end
        if sum(cellfun(@strcmp,liste_comp_metab2,{'Cytoplasm','Mitochondrion_interspace'})) == 2
            Reac.(idR).localisation = 'Mitochondrion_outer_membrane';
        end
        if sum(cellfun(@strcmp,liste_comp_metab2,{'Mitochondrion_interspace','Mitochondrion_matrix'})) == 2
            Reac.(idR).localisation = 'Mitochondrion_inner_membrane';
        end

    elseif length(liste_comp_name_metab) == 1
        Reac.(idR).localisation = liste_comp_name_metab{j};
        
    elseif length(liste_comp_name_metab) == 3
        % cas particulier du transport de phosphate
        Reac.(idR).localisation = 'Cytoplasm'; 
        idR
    else
        Reac.(idR).localisation = 'Cytoplasm'; 
         
    end
    liste_comp_name_metab = [];
    liste_comp_metab = [];
    liste_comp_metab2 = [];
end



