function [RBAmodel] = build_RBAmodel_eukaryotes(Init_RBA)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Research software: PlantCellRBA
% @ Copyright (c) INRAE
% Author: Anne Goelzer (INRAE), anne.goelzer@inrae.fr
% LICENCE: EUPL v1.2 (https://eupl.eu/1.2/en/)
% Requires: MATLAB and IBM ILOG CPLEX (v12.6 or v12.8) licences for code running
% Creation and last modification: 12 May 2023
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%

%-------------
% struc RBA
%------------
Pm = Init_RBA.Pm;
ProcessFull = Init_RBA.ProcessFull;
Medium = Init_RBA.Medium;
S = Init_RBA.S;
status_nu = Init_RBA.status_nu;
Comp = Init_RBA.Comp;
Metab = Init_RBA.Metab;
Reac = Init_RBA.Reac;
RNA = Init_RBA.RNA;
DNA = Init_RBA.DNA;
TargetsMF = Init_RBA.TargetsMF;
Prot = Init_RBA.Prot;
ProcessingMaps = Init_RBA.ProcessingMaps;

% Targets
TargetMC= Init_RBA.TargetMC;

% Variables
listePm = fieldnames(Pm);
Nm = length(listePm);
ind_duplicate = [];
ind_correspondance_enz_duplicate = [];
k = 1;
for i=1:Nm
   if ~isempty(strfind(listePm{i},'duplicate'))
       ind_duplicate = [ind_duplicate i];
       ind_correspondance_enz_duplicate = [ind_correspondance_enz_duplicate; save_i i];
   else
       listePm_ssduplicate{k} = listePm{i};
       save_i = i;
       ind_correspondance_enz_duplicate = [ind_correspondance_enz_duplicate; save_i i];
       k = k+1;
   end
end
ind_enz_unique = unique(ind_correspondance_enz_duplicate(:,1));


% Build Process list for the ones having an associated MM 
listePFull = fieldnames(ProcessFull);
NproFull = length(listePFull);
for i=1:NproFull
   if ~isempty(ProcessFull.(listePFull{i}).MM) 
       Process.(listePFull{i}) = ProcessFull.(listePFull{i});
   else
       ProcessZeroCost.(listePFull{i}) = ProcessFull.(listePFull{i});
   end
end
listeProcess = fieldnames(Process);
listeProcessZero = fieldnames(ProcessZeroCost);
Npro = length(listeProcess);
NproZ = length(listeProcessZero);
Nfz = 0;
for i=1:NproZ
    Nfz = Nfz + length(ProcessZeroCost.(listeProcessZero{i}).listOfTargets);
end


% Targets
IdTargetMC = fieldnames(TargetMC);
Ntg = length(IdTargetMC);

% MetabTargetMC
listeMetab = fieldnames(Metab);
Nx = length(listeMetab);

% Reac
listeReac = fieldnames(Reac);
Nr = length(listeReac);
if Nr ~= Nm
   warning('nbre of reaction and enzymes different... Please check ....') 
end
k = 1;
for i=1:Nm
   if isempty(strfind(listeReac{i},'duplicate'))
       listeReac_ssduplicate{k} = listeReac{i};
       k = k+1;
   end
end

% Compartments + density variables
% 1 decision variable per compartment
listeComp = fieldnames(Comp);
Nc = length(listeComp);
for i=1:Nc
   if ~strcmp(listeComp{i},'Secreted') 
       CompDensity.(listeComp{i}) = Comp.(listeComp{i});
   end
end
listeCompD = fieldnames(CompDensity);
Ncd = length(listeCompD);


% variables 
% [u_Y u_T Y_i T_i nu_i f_i]
Ny = Npro + Nm;
Nyt = Ny + Ntg;
Nv = Nyt*2 + Nm + Nc;
offset_concentration = Nyt;
offset_flux = Nyt*2;
offset_volume = Nyt*2 + Nm;
offset_target_concentration = Nyt + Ny;

% indexes in Aeq
% Ncf = nbre of contraintes on volumes
Ncf = 1;
ind_eq_f = 0; 
ind_eq_X = ind_eq_f + Ncf;
ind_eq_Y = ind_eq_X + Nx;
ind_eq_T = ind_eq_Y + Ny;
ind_eq_Tfix = ind_eq_T + Ntg;
nAeq = ind_eq_Tfix + Ntg; % 4 tgcar lien sur la somme des volumes, et 3 contraintes mitochondrie

% indexes in A
ind_iq_D = 0;
ind_iq_Pro = ind_iq_D + Nc;
ind_iq_nu =  ind_iq_Pro + Npro;
nA = ind_iq_nu + Nm*2;

% objective fonction
% [u_Y u_T Y_i T_i nu_i f_i ]
f  = sparse([ zeros(Nyt,1); ones(Ny,1); zeros(Nm +Nc+Ntg,1)]); % cplex min somme des E


%-----------------------------------------
% init optimisation
%-----------------------------------------
% Bound on variables

LB= sparse(zeros(Nv,1));
% flux reversible has 1 in status_nu
LB(offset_flux+(1:Nm)) = -1000*status_nu;
UB= sparse(1e5*ones(Nv,1));




    
% ----------------------------------------------------------------
% Construction of the optimization problem
%-----------------------------------------------------------------
% We will build matrixes A0, A1, Aeq0, Aeq1
% b0, b1 et beq0 beq1 such that:
% A = A0 + A1*µ
% b = b0 + b1*µ
% Aeq = Aeq0 + Aeq1*µ
% beq = beq0 + beq1*µ
A0 = sparse(zeros(nA,Nv));
A1 = sparse(zeros(nA,Nv));
b0 = sparse(zeros(nA,1));
b1 = sparse(zeros(nA,1));
Aeq0 = sparse(zeros(nAeq,Nv));
Aeq1 = sparse(zeros(nAeq,Nv));
beq0 = sparse(zeros(nAeq,1));
beq1 = sparse(zeros(nAeq,1));

%-------------------------------------------------
% Process capacity constraints
% RBA C2a
% µ(u_Y + T) <= K_eff Y
% alternatively u_Y + u_T <= K_eff Y
%-------------------------------------------------
for i=1:Npro
    % Enzymes + transporters
    for j=1:Nm
        A0(ind_iq_Pro+i,j) = A0(ind_iq_Pro+i,j) + split_capacity_process_machine(Pm.(listePm{j}).MM,Process.(listeProcess{i}));
        
    end
    
    % Macromolecular processes MM 
    for k=1:Npro
        A0(ind_iq_Pro+i,Nm+k) = A0(ind_iq_Pro+i,Nm+k) + split_capacity_process_machine(Process.(listeProcess{k}).MM,Process.(listeProcess{i}));
    end
    
     % Targets
    for k=1:Ntg
        A0(ind_iq_Pro+i,Ny+k) = A0(ind_iq_Pro+i,Ny+k) + split_capacity_process_molecule(TargetMC.(IdTargetMC{k}),Process.(listeProcess{i}));
    end
    
    
end



%-------------------------------------------------------------------
% Cytosolic density constraint: C_3a and C_3b
% Y + T <= f_i
% be careful: the cytoplasm = 1-sum_i f_i
% the f_i part is added later
% Here the constraints involve the CONCENTRATIONS
%-------------------------------------------------------------------
% 1 constraint per compartment having a density constraints 
% work on CompDensity
% Treat enzymes and transporters 
for i=1:Nm
    % Look at the localisation of proteins composing the enzymes 
    % Add the protein to the corresponding constraint
    idE = listePm{i};
    
    % if MM 
    if ~isempty(Pm.(idE).MM.idC)
        for j=1:length(Pm.(idE).MM.idC)
            % id component
            idC = Pm.(idE).MM.idC{j};
            % id compartment
            if isfield(Pm.(idE).MM.Prot,idC)
                idComp =  Pm.(idE).MM.Prot.(idC).localisation;
                % index density constraint
                id_C_density = max(strcmp(idComp,listeComp).*[1:Nc]');
                if id_C_density>0
                    A0(ind_iq_D+id_C_density,offset_concentration+i) = A0(ind_iq_D+id_C_density,offset_concentration+i) + Pm.(idE).MM.Prot.(idC).density_aa*Pm.(idE).MM.stoichiometry(j);
                end
            end
        end
    end
end
    
% Treat machines of macromolecular processes 
for i=1:Npro
    idP = listeProcess{i};
    if isfield(Process.(idP).MM,'Prot')
        lP = fieldnames(Process.(idP).MM.Prot);
        for j=1:length(lP)
            idProt = lP{j};
            % stoichiometry 
            idPPP = max(strcmp(idProt,Process.(idP).MM.idC).*[1:length(Process.(idP).MM.idC)]);
            cstoe = Process.(idP).MM.stoichiometry(idPPP);
            % localisation
            idC =  Process.(idP).MM.Prot.(idProt).localisation;
            id_C_density = max(strcmp(idC,listeComp).*[1:Nc]');
            if id_C_density>0
                A0(ind_iq_D+id_C_density,offset_concentration+Nm+i) = A0(ind_iq_D+id_C_density,offset_concentration+Nm+i) +  Process.(idP).MM.Prot.(idProt).density_aa*cstoe;
            end
            clear cstoe idC id_C_density idPPP idProt
        end
    end
    if isfield(Process.(idP).MM,'RNA')
        lR = fieldnames(Process.(idP).MM.RNA);
        for j=1:length(lR)
            idRNA = lR{j};
            % stoichiometry 
            idRRR = max(strcmp(idRNA,Process.(idP).MM.idC).*[1:length(Process.(idP).MM.idC)]);
            cstoe = Process.(idP).MM.stoichiometry(idRRR);
            % localisation
            idC =  Process.(idP).MM.RNA.(idRNA).localisation;
            id_C_density = max(strcmp(idC,listeComp).*[1:Nc]');
            if id_C_density>0
                A0(ind_iq_D+id_C_density,offset_concentration+Nm+i) = A0(ind_iq_D+id_C_density,offset_concentration+Nm+i) +  Process.(idP).MM.RNA.(idRNA).composition.density*cstoe;
            end
            clear cstoe idC id_C_density idRRR idRNA
        end
    end
end


%Treat targets
for i=1:Ntg
    idT = IdTargetMC{i};
    idC = TargetMC.(idT).localisation;
    id_C_density = max(strcmp(idC,listeComp).*[1:Nc]');
    if id_C_density>0
        A0(ind_iq_D+id_C_density,offset_concentration+Ny+i) = A0(ind_iq_D+id_C_density,offset_concentration+Ny+i) +  TargetMC.(idT).density_aa;
    end
end


% -----------------------------------------------------------

% Constraints linking volumes : the cytoplasm = 1-sum_i f_i  
Aeq0(ind_eq_f + 1,offset_volume + [1:Nc]) = ones(1,Nc);
beq0(ind_eq_f + 1,1) = 1;

%--------------------------------------------------------------------

%----------------------------------------------
% Metabolic constraints C1
%----------------------------------------------
%----------------------------------------------------------------------
% Metabolic constraint 1
% Sv - (C_XY u_Y + C_XB u_B + C_XT u_T)  = 0
%----------------------------------------------------------------------
% Stoichiometry matrix Sv
Aeq0(ind_eq_X + [1:Nx],offset_flux+[1:Nm]) = S;

%--------------------------------------------------
% Add processing costs for molecular machines
% Term µ C_XY u_Y
% Treat enzymes and transporters 
for i=1:Nm
    % Compute cost
    vecCostMM = split_cost_machine(Pm.(listePm{i}).MM,ProcessFull, Metab,'P');
    
    % Add cost on Aeq 
    Aeq0(ind_eq_X + [1:Nx],i) = Aeq0(ind_eq_X + [1:Nx],i) + vecCostMM;
        
end

% % Treat machines of macromolecular processes 
% Term  C_XY u_Y
for i=1:Npro

    % Compute cost
    vecCostMM = split_cost_machine(Process.(listeProcess{i}).MM,ProcessFull, Metab,'P');
    
    % Add cost on Aeq 
    Aeq0(ind_eq_X + [1:Nx],Nm+i) = Aeq0(ind_eq_X + [1:Nx],Nm+i) + vecCostMM;

end
    

% Treat Targets
for i=1:Ntg
    idT = IdTargetMC{i};
    try
        % Compute cost
        vecCostM_prod = split_cost_molecule(TargetMC.(idT).id,TargetMC.(idT),ProcessFull, Metab,'P');
        % Add cost on Aeq 
        Aeq0(ind_eq_X + [1:Nx],Ny+i) = Aeq0(ind_eq_X + [1:Nx],Ny+i) + vecCostM_prod;      
        
    catch
        disp('pb target')
        idT
        keyboard
    end
        
end


%-------------------------------------------------
% Process capacity constraints
% RBA C2b
% vi - k_Ef*E <0
% -(vi+k_Eb.E)<0
% Matrice A
%--------------------------

for i=1:Nm
    idE = listePm{i};
    A0(ind_iq_nu+i,offset_concentration+i) = -Pm.(idE).keff_f;
    A0(ind_iq_nu+i,offset_flux+i) = 1;
    A0(ind_iq_nu+Nm+i,offset_concentration+i) = -Pm.(idE).keff_b;
    A0(ind_iq_nu+Nm+i,offset_flux+i) = -1;
end


% Add constraints: 
% u_Y - (µ)Y = 0
Aeq0(ind_eq_Y + [1:Ny],[1:Ny]) = eye(Ny);
Aeq1(ind_eq_Y + [1:Ny],offset_concentration+[1:Ny]) = -speye(Ny);


% u_T - (µ)T = 0
name_Aeq_TT = {};
cpt = 1;
ind_uT = [];
for i=1:Ntg
    idMC =  IdTargetMC{i};
     if isfield(Metab,idMC)
         Aeq0(ind_eq_X + Metab.(idMC).number_S,Ny + i) = Aeq0(ind_eq_X + Metab.(idMC).number_S,Ny + i) + 1;
         Aeq1(ind_eq_X + Metab.(idMC).number_S,offset_concentration + Ny + i) = Aeq1(ind_eq_X + Metab.(idMC).number_S,offset_concentration + Ny + i) -1;

         ind_uT = [ind_uT ind_eq_X + Metab.(idMC).number_S];
     else      
         Aeq0(ind_eq_T + cpt,Ny + i) = 1;
         Aeq1(ind_eq_T + cpt,offset_concentration + Ny + i) = -1;

         ind_uT = [ind_uT ind_eq_T + cpt];
         
         name_Aeq_TT{cpt} = ['YT_' idMC];
         cpt = cpt + 1;
     end
end
Nlig_add = cpt-1;
clear cpt

% Add constraints on targets
Aeq0(ind_eq_Tfix+[1:Ntg],offset_target_concentration+[1:Ntg]) = eye(Ntg);

% name of variables and constraints
k=1;
for i=1:Nm,  
    tutu{i} = ['Capacity_forward_']; 
    tata{i} = ['Capacity_backward_'];  
    tyty{i} = ['nu_']; 
    plop{i} = ['Y_']; 
    plup{i} = ['u_'];  
     if isempty(strfind(listePm{i},'duplicate'))
         lala{k} = ['Y_'];
         lulu{k} = ['nu_'];
            k = k+1;
     end
end
CFPM = cellfun(@strcat,tutu,{listePm{:}},'UniformOutput',false); clear tutu
CBPM = cellfun(@strcat,tata,{listePm{:}},'UniformOutput',false); clear tata
CPM = cellfun(@strcat,plop,{listePm{:}},'UniformOutput',false); clear plop
CPM_ssd = cellfun(@strcat,lala,{listePm_ssduplicate{:}},'UniformOutput',false); clear lala
UPM = cellfun(@strcat,plup,{listePm{:}},'UniformOutput',false); clear plup
NUPM = cellfun(@strcat,tyty,{listeReac{:}},'UniformOutput',false); clear tyty
NUPM_ssd = cellfun(@strcat,lulu,{listeReac_ssduplicate{:}},'UniformOutput',false); clear lulu

for i=1:Npro, toto{i} = ['Y_']; tutu{i} = ['Capacity_']; tata{i} = ['u_']; end
MMPR = cellfun(@strcat,toto,{listeProcess{:}},'UniformOutput',false); clear toto
CPR = cellfun(@strcat,tutu,{listeProcess{:}},'UniformOutput',false); clear tutu
UPR = cellfun(@strcat,tata,{listeProcess{:}},'UniformOutput',false); clear tata

for i=1:Nc, toto{i} = ['Capacity_compartment_']; titi{i} = ['f_']; end
CComp = cellfun(@strcat,toto,{listeComp{:}},'UniformOutput',false); clear toto
FComp = cellfun(@strcat,titi,{listeComp{:}},'UniformOutput',false); clear titi

for i=1:Ntg,  titi{i} = ['u_']; tutu{i} = ['T_']; end
UTG = cellfun(@strcat,titi,{IdTargetMC{:}},'UniformOutput',false); clear titi
CTG = cellfun(@strcat,tutu,{IdTargetMC{:}},'UniformOutput',false); clear tutu


name_Xv = {UPM{:}, UPR{:},UTG{:},CPM{:}, MMPR{:},CTG{:}, NUPM{:}, FComp{:}};
name_Aeq = {'Sum_f',  listeMetab{:}, CPM{:},MMPR{:}, name_Aeq_TT{:},IdTargetMC{:}};
name_A = {CComp{:},CPR{:},CFPM{:},CBPM{:}};

% Build Matrices structures 
Matrices.Aeq0 = Aeq0;
Matrices.Aeq1 = Aeq1;
Matrices.A0 = A0;
Matrices.A1 = A1;
Matrices.beq0 = beq0;
Matrices.beq1 = beq1;
Matrices.b0 = b0;
Matrices.b1 = b1;
Matrices.name_Aeq = name_Aeq;
Matrices.name_A = name_A;
Matrices.name_Xv = name_Xv;
Matrices.LB = LB;
Matrices.UB = UB;
Matrices.f = f;
%Add constraints on fixed targets ad fluxes 
Matrices = addFluxFix('maintenance_atp_target',offset_flux,TargetsMF,Reac,Matrices);

% Build new matrixes with merged complexes when isoenzymes
A0_mean_complex = Matrices.A0;
Aeq0_mean_complex = Matrices.Aeq0;

for lll = 1:length(ind_enz_unique)
    indGG = find(ind_correspondance_enz_duplicate(:,1) == ind_enz_unique(lll));
    ind_duplicate_enz_lll = ind_correspondance_enz_duplicate(indGG,2);
    ind_enz_1 = ind_duplicate_enz_lll(1);
    % mean of columns on A0 on u variables
    bloc = A0_mean_complex(:,ind_duplicate_enz_lll); bloc(bloc == 0) = NaN;
    bloc_m = nanmean(bloc,2); bloc_m(isnan(bloc_m)) = 0;
    bloc_up = A0_mean_complex(:,ind_enz_1); % bloc initial
    % update the MM 
    bloc_up(1:ind_iq_nu,1) = bloc_m(1:ind_iq_nu,1);
    A0_mean_complex(:,ind_enz_1) = bloc_up;
    % mean of columns on Aeq on u variables
    bloc2 = Aeq0_mean_complex(:,ind_duplicate_enz_lll); bloc2(bloc2 == 0) = NaN;
    bloc2_m = nanmean(bloc2,2); bloc2_m(isnan(bloc2_m)) = 0;
    bloc2_up = Aeq0_mean_complex(:,ind_enz_1);% bloc initial
    % update the MM 
    bloc2_up(ind_eq_X+[1:Nx],1) = bloc2_m(ind_eq_X+[1:Nx],1);
    Aeq0_mean_complex(:,ind_enz_1) = bloc2_up;
    clear bloc2 bloc2_m bloc bloc_m bloc2_up bloc_up
    
    % mean of columns on A0 on concentrations
    bloc = A0_mean_complex(:,offset_concentration +ind_duplicate_enz_lll); bloc(bloc == 0) = NaN;
    bloc_m = nanmean(bloc,2); bloc_m(isnan(bloc_m)) = 0;
    bloc_up = A0_mean_complex(:,offset_concentration +ind_enz_1); % bloc initial
    % update the MM 
    bloc_up(1:ind_iq_nu,1) = bloc_m(1:ind_iq_nu,1);
    A0_mean_complex(:,offset_concentration +ind_enz_1) = bloc_up;
    % mean of columns on Aeq on concentrations
    bloc2 = Aeq0_mean_complex(:,offset_concentration +ind_duplicate_enz_lll); bloc2(bloc2 == 0) = NaN;
    bloc2_m = nanmean(bloc2,2); bloc2_m(isnan(bloc2_m)) = 0;
    bloc2_up = Aeq0_mean_complex(:,offset_concentration +ind_enz_1);% bloc initial
    % update the MM 
    bloc2_up(ind_eq_X+[1:Nx],1) = bloc2_m(ind_eq_X+[1:Nx],1);
    Aeq0_mean_complex(:,offset_concentration +ind_enz_1) = bloc2_up;
    clear bloc2 bloc2_m bloc bloc_m
     
end

Matrices.Aeq0_mean_complex = Aeq0_mean_complex;
Matrices.A0_mean_complex = A0_mean_complex;

% save indexes
Sind.Nm = Nm;
Sind.Nc= Nc;
Sind.offset_flux = offset_flux;
Sind.offset_target_concentration = offset_target_concentration;
Sind.offset_concentration = offset_concentration;
Sind.offset_volume = offset_volume;
Sind.ind_iq_D = ind_iq_D;
Sind.ind_iq_Pro = ind_iq_Pro;
Sind.ind_iq_nu = ind_iq_nu;
Sind.ind_eq_X = ind_eq_X;
Sind.ind_eq_Y = ind_eq_Y;
Sind.ind_eq_T = ind_eq_T;
Sind.ind_eq_f = ind_eq_f;
Sind.Npro = Npro;
Sind.Nx = Nx;
Sind.Ntg = Ntg;
Sind.Ny = Ny;
Sind.ind_uT = ind_uT;
Sind.ind_eq_Tfix = ind_eq_Tfix;
Sind.ind_duplicate = ind_duplicate;
Sind.ind_correspondance_enz_duplicate = ind_correspondance_enz_duplicate;
Sind.ind_enz_unique = ind_enz_unique;
Sind.Nlig_add = Nlig_add;

% return model
RBAmodel.matrix = Matrices;
RBAmodel.Sind = Sind;
RBAmodel.Process = Process;
RBAmodel.Pm = Pm;

