%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% script simulating the RBA model for varying CO2, O2, Temperature and Irradiance
% Simulations will be compared to the simulations of the Farqhuar model
% on Figure 7
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Research software: PlantCellRBA
% @ Copyright (c) INRAE
% Author: Anne Goelzer (INRAE), anne.goelzer@inrae.fr
% LICENCE: EUPL v1.2 (https://eupl.eu/1.2/en/)
% Requires: MATLAB and IBM ILOG CPLEX (v12.6 or v12.8) licences for code running
% Creation and last modification: 12 May 2023
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% This line needs to be updated with the path to CPLEX
% addpath My/path/to/Cplex
addpath ./visualisation/
addpath ./utils/
addpath ./RBA_xml/
addpath ./RBA_mat/
clear all;
close all

disp('---- Init RBA ----')
% load prot, RNA, DNA
load RBA_mat/Prot_RNA_DNA_forRBA
    
% load Pm Process
load RBA_mat/mol_machines
    
% load metabolism: compartments, Reac, Metab S
load RBA_mat/metabolism
    
% load targets:TargetsMF TargetDensities
load RBA_mat/targets
    
%  load Parameters: Parameters ParameterFunctions
load RBA_mat/parameters

%----------------------------------------------------    

% Report functional annotation 
[Reac,Subsystem,Subsystem_curated] = report_functional_annotation_from_FBA(Reac, 'RBA_mat/AraCore.mat');


PhysioPlante.gDW_gFW = 8.35/100; % fraction of DW in FW
PhysioPlante.protein_content_mg_FW = 18.97; % mg/gFW
PhysioPlante.SLA = 31.6/1000; % specific leaf area. m2/kgDW converted in m2/gDW in Pyl et al. 2012
PhysioPlante.day_length = 8; % h

%-----------------------------------------------
% Initialisation of the medium from the file medium.tsv in RBA-xml
% directory
Medium = import_medium('e');
Medium.M_CO2_e = 200e-3/1.01325; % 50 to 400 umol
Medium.M_O2_e = 210/1.01325;
Medium.M_hnu_e = 1000; %umol
Medium.M_Suc_e =0;
Temperature_celsius = 21;

% update the structure functions with medium
ParameterFunctions_m = update_function_medium(ParameterFunctions,Medium);


% Update transporter efficiencies with medium
Pm = update_max_efficiency(Pm,Parameters,ParameterFunctions_m);

% on elimine les met boundary dans la matrices de stoichiometry
S_full = S;
S(id_ex,:) = [];

% on update la structure
MetabFull = Metab;
clear Metab

listeMetabFull = fieldnames(MetabFull);
NMetFull = length(listeMetabFull);
cpt = 1;
for i=1:NMetFull
    % test si machine non nulle
    if MetabFull.(listeMetabFull{i}).boundaryCondition == 0
        Metab.(listeMetabFull{i}) = MetabFull.(listeMetabFull{i});
        Metab.(listeMetabFull{i}).number_S = cpt;
        cpt = cpt+1;
    end
end

% option of simulation
optionsRBA.seuil_mu = 1e-4;
optionsRBA.display_cplex = 'off';
optionsRBA.display_iter_mu = 'off';


% init
Init_RBA.Pm = Pm;
Init_RBA.ProcessFull = ProcessFull;
Init_RBA.ListeAllElementMachine = ListeAllElementMachine;
Init_RBA.Medium = Medium;
Init_RBA.S_full = S_full;
Init_RBA.S = S;
Init_RBA.status_nu = status_nu;
Init_RBA.id_met_boundary = id_ex;
Init_RBA.Comp = Comp;
Init_RBA.Metab = Metab;
Init_RBA.Reac = Reac;
Init_RBA.RNA = RNA;
Init_RBA.DNA = DNA;
Init_RBA.ParameterFunctions_m = ParameterFunctions_m;
Init_RBA.Parameters = Parameters;
Init_RBA.TargetsMF = TargetsMF;
Init_RBA.TargetsDensities = TargetDensities;
Init_RBA.TargetMC = TargetMC;
Init_RBA.TargetMetab = TargetMetab;
Init_RBA.IdTargetReac = IdTargetReac;
Init_RBA.IdAllTargetSpecies = IdAllSpecies;
Init_RBA.ProcessingMaps = ProcessingMaps;
Init_RBA.Subsystem = Subsystem;
Init_RBA.Subsystem_curated = Subsystem_curated;
Init_RBA.Prot = Prot;



disp('---- Build model  ----')
[RBAmodel] = build_RBAmodel_eukaryotes(Init_RBA);
% save the original model
ParameterFunctions_m_original = ParameterFunctions_m;

scenario = 'IC';  % IC, TC or TO
Init_RBA.scenario = scenario;
gridT = [0 5 10 15 20 21 25 30 35 40 45 50 55 60 ]; % la plage de valeur pour la temperature
gridO2 = [10 50 100 150 210 250 330]/1.01325; % la plage de valeur pour l'O2
gridCO2 = [55 110 165 200 230 330]*1e-3/1.01325; % la plage de valeur pour le CO2
gridI = [0:20:200 500:500:2000]*1e-3*PhysioPlante.SLA*3600*PhysioPlante.day_length; % flux µmol/m2/s -> en mM/gDW/day of 8h
switch scenario
    case 'TC'
        type_subs1_vary = 'CO2';
        type_subs2_vary = 'Temperature';
        unit1 = 'ubar';
        unit2 = '°C';
        grid_simul_1 =gridCO2;
        grid_simul_2 =gridT;
        fact_mul1_for_display = 1.01325/1e-3;
        fact_mul2_for_display = 1;
        str_title = 'Variation of CO2 and temperature';
        
    case 'TO'
        type_subs1_vary = 'O2';
        type_subs2_vary = 'Temperature';
        unit1 = 'mbar';
        unit2 = '°C';
        grid_simul_1 =gridO2;
        grid_simul_2 =gridT;
        fact_mul1_for_display = 1.01325;
        fact_mul2_for_display = 1;
        str_title = 'Variation of O2 and temperature';
        
    case 'IC'
        type_subs1_vary = 'CO2';
        type_subs2_vary = 'Irradiance';
        unit1 = 'ubar';
        unit2 = 'umol/m2/s';
        grid_simul_1 =gridCO2;
        grid_simul_2 =gridI;
        fact_mul1_for_display = 1.01325/1e-3;
        fact_mul2_for_display = 1/1e-3/PhysioPlante.SLA/3600/PhysioPlante.day_length;
        str_title = 'Variation of CO2 and irradiance';
        
       
        
end

vecC = ['brkmcgy'];

for i= 1:length(grid_simul_1)
    labels_legend{i} = [type_subs1_vary '= ' num2str(grid_simul_1(i)*fact_mul1_for_display) ' ' unit1] ;
    for j=1:length(grid_simul_2)
    
        disp([type_subs1_vary ' (mM): ' num2str(grid_simul_1(i)) ', ' type_subs2_vary '(' unit2 '): ' num2str(grid_simul_2(j))])
    % 
    RBAmodel_iter = RBAmodel;
    
    % on update le milieu
    switch scenario
        case 'TC'
        Medium.M_CO2_e =grid_simul_1(i);   
        
        case 'TO'
        Medium.M_O2_e =grid_simul_1(i);
        
        case 'IC'
        Medium.M_hnu_e = grid_simul_2(j); % amount in mM/gdW of photons in 8h
        Medium.M_CO2_e = grid_simul_1(i);
        % et ici on rajoute une contrainte UB(h_nu) < gridI
        oo = max(strcmp('nu_R_Im_hnu',RBAmodel_iter.matrix.name_Xv).*[1:length(RBAmodel_iter.matrix.name_Xv)]);
        RBAmodel_iter.matrix.UB(oo) = grid_simul_2(j); % limitation of the influx of photons
        clear oo
    end
    
    
    % update the structure functions with medium 
    ParameterFunctions_m_update = update_function_medium(ParameterFunctions_m_original,Medium);
    
    
    % Update transporter efficiency     
    RBAmodel_iter.Pm = update_max_efficiency(RBAmodel_iter.Pm,Parameters,ParameterFunctions_m_update);
    
    % % Update Rubisco efficiency with medium 
    switch scenario
        case {'TC','TO'}
            RBAmodel_iter.Pm = update_efficiency_rubisco(RBAmodel_iter.Pm,Medium.M_CO2_e,Medium.M_O2_e,Medium.M_hnu_e,grid_simul_2(j));
            
        case {'IC'}
            % here we keep T=25°C as in Biochemical Model of C3 Photosynthesis
            RBAmodel_iter.Pm = update_efficiency_rubisco(RBAmodel_iter.Pm,Medium.M_CO2_e,Medium.M_O2_e,Medium.M_hnu_e,25);

    end
    
    Nm = RBAmodel_iter.Sind.Nm;
    listePm = fieldnames(RBAmodel_iter.Pm);
    % et maintenant on update uniquement la matrice A0
    for k=1:Nm
        idE = listePm{k};
        % A0
        RBAmodel_iter.matrix.A0(RBAmodel_iter.Sind.ind_iq_nu+k,RBAmodel_iter.Sind.offset_concentration+k) = -RBAmodel_iter.Pm.(idE).keff_f;
        RBAmodel_iter.matrix.A0(RBAmodel_iter.Sind.ind_iq_nu+k,RBAmodel_iter.Sind.offset_flux+k) = 1;
        RBAmodel_iter.matrix.A0(RBAmodel_iter.Sind.ind_iq_nu+Nm+k,RBAmodel_iter.Sind.offset_concentration+k) = -RBAmodel_iter.Pm.(idE).keff_b;
        RBAmodel_iter.matrix.A0(RBAmodel_iter.Sind.ind_iq_nu+Nm+k,RBAmodel_iter.Sind.offset_flux+k) = -1;
        % A0_mean_complex
        RBAmodel_iter.matrix.A0_mean_complex(RBAmodel_iter.Sind.ind_iq_nu+k,RBAmodel_iter.Sind.offset_concentration+k) = -RBAmodel_iter.Pm.(idE).keff_f;
        RBAmodel_iter.matrix.A0_mean_complex(RBAmodel_iter.Sind.ind_iq_nu+k,RBAmodel_iter.Sind.offset_flux+k) = 1;
        RBAmodel_iter.matrix.A0_mean_complex(RBAmodel_iter.Sind.ind_iq_nu+Nm+k,RBAmodel_iter.Sind.offset_concentration+k) = -RBAmodel_iter.Pm.(idE).keff_b;
        RBAmodel_iter.matrix.A0_mean_complex(RBAmodel_iter.Sind.ind_iq_nu+Nm+k,RBAmodel_iter.Sind.offset_flux+k) = -1;
    end
    
    % update ParameterFunctions_m
    Init_RBA.ParameterFunctions_m = ParameterFunctions_m_update;
    
    disp('---- Look for µ  ----')
    [RBAopt] = solve_RBAmodel_eukaryotes(RBAmodel_iter,Init_RBA,optionsRBA);
    if ~isempty(RBAopt)
        SolRBA{i}.X(:,j) = RBAopt.X2;
        SolRBA{i}.mu(j) = RBAopt.mu_opt;
        SolRBA{i}.sol = RBAopt;
        name_Xv2 = RBAopt.name_Xv2;
    end

        clear RBAmodel_iter ParameterFunctions_m_update
    end  
    
    cc = max(strcmp('nu_R_Im_CO2',name_Xv2).*[1:length(name_Xv2)]);
    figure(1)
    plot(grid_simul_2*fact_mul2_for_display,SolRBA{i}.X(cc,:),[vecC(i) '+-'])
    hold on

    
    figure(2)
    plot(grid_simul_2*fact_mul2_for_display,SolRBA{i}.mu,[vecC(i) '+-'])
    hold on
    
end

figure(1)
grid on
title(str_title)
legend(labels_legend,'Location','northwest')
xlabel([ type_subs2_vary ' ' unit2 ])
ylabel('CO2 assimilation rate (mM/gDW/d)')
    
figure(2)
grid on
title(str_title)
legend(labels_legend,'Location','northwest')
xlabel([ type_subs2_vary ' ' unit2 ])
ylabel('Relative growth rate, 1/d')
    

%eval(['save simulation/Farquhar/RBA_vary' scenario ' scenario SolRBA PhysioPlante labels_legend gridT gridO2 gridCO2 gridI'])


