function write_result(RBAopt,Init_RBA)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Research software: PlantCellRBA
% @ Copyright (c) INRAE
% Author: Anne Goelzer (INRAE), anne.goelzer@inrae.fr
% LICENCE: EUPL v1.2 (https://eupl.eu/1.2/en/)
% Requires: MATLAB and IBM ILOG CPLEX (v12.6 or v12.8) licences for code running
% Creation and last modification: 12 May 2023
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
id_in = cellfun(@(x)startsWith(x,'nu_R_PSII_h'),RBAopt.name_Xv2,'UniformOutput',0);
idIn = find(cell2mat(id_in)~=0);
id_out = cellfun(@(x)startsWith(x,'f_Cell_membrane'),RBAopt.name_Xv2,'UniformOutput',0);
idOut = find(cell2mat(id_out)~=0);

vecIn = idIn:(idOut-1);
Flux = RBAopt.X2(vecIn);
label_flux = RBAopt.name_Xv2(vecIn);
%--------------------------------------------
fp = fopen('Flux_distribution.csv','w');
fprintf(fp,'%s;%s;%s;%s\n','Pathway','ID Reaction', 'Chemical reaction', 'Flux value (mM/gDW/day)');

for i=1:length(vecIn)
    id_R = RBAopt.name_Xv2{vecIn(i)};
    id_R = strrep(id_R,'nu_','');
    str_reac = [];
    for j=1:length(Init_RBA.Reac.(id_R).reactant)
        str_reac = [str_reac ' ' num2str(Init_RBA.Reac.(id_R).reactant{j}.stoichiometry) ' ' Init_RBA.MetabFull.(Init_RBA.Reac.(id_R).reactant{j}.species).name ' +' ];
    end
    % substitute the last '+' by  => ou <=>
    switch Init_RBA.Reac.(id_R).reversible
        
        case 0
            % irreversible
            if ~isempty(str_reac)
                str_reac = [str_reac(1:end-1) '=>'];
            else
                str_reac = '=>';
            end
        case 1
            % reversible
            if ~isempty(str_reac)
                str_reac = [str_reac(1:end-1) '<=>'];
            else
                str_reac = '<=>';
            end
    end
    for j=1:length(Init_RBA.Reac.(id_R).product)
        str_reac = [str_reac ' ' num2str(Init_RBA.Reac.(id_R).product{j}.stoichiometry) ' ' Init_RBA.MetabFull.(Init_RBA.Reac.(id_R).product{j}.species).name ' +' ];
    end
    % remove '+'
    str_reac(end)=[];
   fprintf(fp,'%s;%s;%s;%6.2d\n',Init_RBA.Reac.(id_R).subsystem_curated, char(RBAopt.name_Xv2(vecIn(i))), str_reac, Flux(i));
end

fclose(fp)
