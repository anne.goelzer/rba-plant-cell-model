%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Research software: PlantCellRBA
% @ Copyright (c) INRAE
% Author: Anne Goelzer (INRAE), anne.goelzer@inrae.fr
% LICENCE: EUPL v1.2 (https://eupl.eu/1.2/en/)
% Requires: MATLAB and IBM ILOG CPLEX (v12.6 or v12.8) licences for code running
% Creation and last modification: 12 May 2023
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function trace_repartition_prot_path(labely,Mat_aa, FaceColor,WidthBar,FontSize)


AA_tot = sum(Mat_aa);

[Mat_aa_sort,ii] = sort(Mat_aa);
labely_sort = labely(ii);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Trace pathway
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
n_path_trace = length(labely);
Mat_aa_norm = Mat_aa*100/AA_tot;
Mat_aa_norm_sort = Mat_aa_sort*100/AA_tot;


figure(100)
hh_g=barh([1:n_path_trace],log10([Mat_aa_norm_sort]),WidthBar,'facecolor', FaceColor);
set(gca,'YTick',[1:n_path_trace],'YTickLabel',labely_sort,'YLim', [0 n_path_trace+1],'FontSize',FontSize)
xlabel('log10 of the repartition (in %) of the total amino acids')
grid on
hold on

