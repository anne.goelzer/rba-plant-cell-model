function display_flux(RBAopt)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Research software: PlantCellRBA
% @ Copyright (c) INRAE
% Author: Anne Goelzer (INRAE), anne.goelzer@inrae.fr
% LICENCE: EUPL v1.2 (https://eupl.eu/1.2/en/)
% Requires: MATLAB and IBM ILOG CPLEX (v12.6 or v12.8) licences for code running
% Creation and last modification: 12 May 2023
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    id_in = cellfun(@(x)startsWith(x,'nu_R_PSII_h'),RBAopt.name_Xv2,'UniformOutput',0);
    idIn = find(cell2mat(id_in)~=0);
    id_out = cellfun(@(x)startsWith(x,'f_Cell_membrane'),RBAopt.name_Xv2,'UniformOutput',0);
    idOut = find(cell2mat(id_out)~=0);
    
    vecIn = idIn:(idOut-1);
  
     disp('Flux (mM/gDW/d)')
    [char(RBAopt.name_Xv2(vecIn)) blanks(length(vecIn))' num2str(RBAopt.X2(vecIn)) ] 
    
    
