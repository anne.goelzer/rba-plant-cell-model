function display_flux_in_out_cell(RBAopt,PhysioPlante)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Research software: PlantCellRBA
% @ Copyright (c) INRAE
% Author: Anne Goelzer (INRAE), anne.goelzer@inrae.fr
% LICENCE: EUPL v1.2 (https://eupl.eu/1.2/en/)
% Requires: MATLAB and IBM ILOG CPLEX (v12.6 or v12.8) licences for code running
% Creation and last modification: 12 May 2023
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    id_in = cellfun(@(x)startsWith(x,'nu_R_Im_'),RBAopt.name_Xv2,'UniformOutput',0);
    vecIn = find(cell2mat(id_in)~=0);
    id_out = cellfun(@(x)startsWith(x,'nu_R_Ex_'),RBAopt.name_Xv2,'UniformOutput',0);
    vecOut = find(cell2mat(id_out)~=0);
    
    
    mm = max(strcmp('nu_R_Si_H',RBAopt.name_Xv2).*[1:length(RBAopt.name_Xv2)]);
    pp = max(strcmp('nu_R_Im_CO2',RBAopt.name_Xv2).*[1:length(RBAopt.name_Xv2)]);
    kk = max(strcmp('nu_R_StS_h2',RBAopt.name_Xv2).*[1:length(RBAopt.name_Xv2)]);
    
    
    disp(sprintf('Flux of H+ (mM/gDW/d): %f ', RBAopt.X2(mm)))
    
    disp('Flux imported (mM/gDW/d)')
    [char(RBAopt.name_Xv2(vecIn)) blanks(length(vecIn))' num2str(RBAopt.X2(vecIn)) ] 
    
     disp('Flux exported (mM/gDW/d)')
    [char(RBAopt.name_Xv2(vecOut)) blanks(length(vecOut))' num2str(RBAopt.X2(vecOut)) ] 
    
    
    disp(sprintf('Net Rate of CO2 assimilation (mM/gDW/d): %f', RBAopt.X2(pp)))
    disp(sprintf('Net Rate of CO2 assimilation (umol/m2/s): %f', RBAopt.X2(pp)*1000/24/3600/PhysioPlante.SLA))
    disp(sprintf('Flux of starch synthesis (mM/gDW/d): %f', RBAopt.X2(kk)))
    
  
    ii = max(strcmp('nu_R_RBC_h',RBAopt.name_Xv2).*[1:length(RBAopt.name_Xv2)]);
    jj = max(strcmp('nu_R_RBO_h',RBAopt.name_Xv2).*[1:length(RBAopt.name_Xv2)]);
    disp(sprintf('Ratio oxygenase/carboxylase activity of rubisco: %f', RBAopt.X2(jj)/RBAopt.X2(ii)))
    
