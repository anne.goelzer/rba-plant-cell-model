%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Research software: PlantCellRBA
% @ Copyright (c) INRAE
% Author: Anne Goelzer (INRAE), anne.goelzer@inrae.fr
% LICENCE: EUPL v1.2 (https://eupl.eu/1.2/en/)
% Requires: MATLAB and IBM ILOG CPLEX (v12.6 or v12.8) licences for code running
% Creation and last modification: 12 May 2023
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [labely_final,Mat_aa_final] = compute_repartition_prot_path(RBAopt,Init_RBA)


 % name + value
nameX= RBAopt.name_Xv2;
X =  RBAopt.X2;

% only concentration of molecular machines 
indY = find(startsWith(nameX,'Y_','IgnoreCase',1)>0);
nameY = nameX(indY);
valY = X(indY);
nY = length(valY);

% 
toto =  find(startsWith(nameY,'Y_P_','IgnoreCase',1)>0);
offset_process = toto(1);


% Structure subsystem containing the user-defined categories
Subsystem = Init_RBA.Subsystem_curated;
listeS = fieldnames(Subsystem);
n_sub = length(listeS);
Mat_aa = zeros(n_sub,1);

for i=1:n_sub
    % determine reaction
   listeR = fieldnames(Subsystem.(listeS{i}));
   nR = length(listeR);
   weight_subsystem = 0;
   
   % special cases
   if strcmpi(listeS{i},'oxidative_phosphorylation')
       pp = find(contains(nameY,'R_cplx1_m','IgnoreCase',1)>0); weight_subsystem = weight_subsystem + valY(pp)*Init_RBA.Pm.(nameY{pp}(3:end)).MM.aa_tot; clear pp;      
       pp = find(contains(nameY,'R_cplx3_m','IgnoreCase',1)>0); weight_subsystem = weight_subsystem + valY(pp)*Init_RBA.Pm.(nameY{pp}(3:end)).MM.aa_tot; clear pp;
       pp = find(contains(nameY,'R_cplx4_m','IgnoreCase',1)>0); weight_subsystem = weight_subsystem + valY(pp)*Init_RBA.Pm.(nameY{pp}(3:end)).MM.aa_tot; clear pp;
       pp = find(contains(nameY,'R_cplx5_m','IgnoreCase',1)>0); weight_subsystem = weight_subsystem + valY(pp)*Init_RBA.Pm.(nameY{pp}(3:end)).MM.aa_tot; clear pp;
       
   elseif strcmpi(listeS{i},'tricarboxylic_acid_cycle')
       pp = find(contains(nameY,'R_cplx2_m','IgnoreCase',1)>0); weight_subsystem = weight_subsystem + valY(pp)*Init_RBA.Pm.(nameY{pp}(3:end)).MM.aa_tot; clear pp;
       pp = find(contains(nameY,'R_CitS_m','IgnoreCase',1)>0); weight_subsystem = weight_subsystem + valY(pp)*Init_RBA.Pm.(nameY{pp}(3:end)).MM.aa_tot; clear pp;
       pp = find(contains(nameY,'R_cACNDHA_m','IgnoreCase',1)>0); weight_subsystem = weight_subsystem + valY(pp)*Init_RBA.Pm.(nameY{pp}(3:end)).MM.aa_tot; clear pp;
       pp = find(contains(nameY,'R_cACNHA_m','IgnoreCase',1)>0); weight_subsystem = weight_subsystem + valY(pp)*Init_RBA.Pm.(nameY{pp}(3:end)).MM.aa_tot; clear pp;       
       pp = find(contains(nameY,'R_MalDH1_m','IgnoreCase',1)>0); weight_subsystem = weight_subsystem + valY(pp)*Init_RBA.Pm.(nameY{pp}(3:end)).MM.aa_tot; clear pp;        
       pp = find(contains(nameY,'R_iCitDHNAD_m','IgnoreCase',1)>0); weight_subsystem = weight_subsystem + valY(pp)*Init_RBA.Pm.(nameY{pp}(3:end)).MM.aa_tot; clear pp;  
       pp = find(contains(nameY,'R_KGDH_m','IgnoreCase',1)>0); weight_subsystem = weight_subsystem + valY(pp)*Init_RBA.Pm.(nameY{pp}(3:end)).MM.aa_tot; clear pp;         
       pp = find(contains(nameY,'R_DHLST_m','IgnoreCase',1)>0); weight_subsystem = weight_subsystem + valY(pp)*Init_RBA.Pm.(nameY{pp}(3:end)).MM.aa_tot; clear pp;   
       pp = find(contains(nameY,'R_SCACoAL_m','IgnoreCase',1)>0); weight_subsystem = weight_subsystem + valY(pp)*Init_RBA.Pm.(nameY{pp}(3:end)).MM.aa_tot; clear pp;        
       pp = find(contains(nameY,'R_FumHA_m','IgnoreCase',1)>0); weight_subsystem = weight_subsystem + valY(pp)*Init_RBA.Pm.(nameY{pp}(3:end)).MM.aa_tot; clear pp;           
       
       
   else
       for j=1:nR
           % careful to duplicated reactions 
           pp = find(contains(nameY,listeR{j},'IgnoreCase',1)>0);
           if length(pp) == 1
               % compute weight of enzymes 
               % nameY starts with Y_R.....
               % eliminate the 2 first characters to have Pm ID 
               if ~isempty(Init_RBA.Pm.(nameY{pp}(3:end)).MM)
                   weight_subsystem = weight_subsystem + valY(pp)*Init_RBA.Pm.(nameY{pp}(3:end)).MM.aa_tot;
               end
           else
               if strcmpi(listeR{j},'R_Tr_PPi')
                   pp = find(strcmpi(nameY,'Y_R_Tr_PPi_enzyme')>0); weight_subsystem = weight_subsystem + valY(pp)*Init_RBA.Pm.(nameY{pp}(3:end)).MM.aa_tot; clear pp;  
               elseif strcmpi(listeR{j},'R_Tr_AAC')
                   pp = find(strcmpi(nameY,'Y_R_Tr_AAC_enzyme')>0); weight_subsystem = weight_subsystem + valY(pp)*Init_RBA.Pm.(nameY{pp}(3:end)).MM.aa_tot; clear pp;  
               end

           end
           
       end
   end
   Mat_aa(i,1) = weight_subsystem;
   clear listeR;
end

labely = listeS;
labely = cellfun(@(x)strrep(x,'_', ' '),labely,'UniformOutput',0);

cpt = n_sub + 1;
% Add process cost 
for i=offset_process:nY
    idProcess = nameY{i}(3:end);
    labely{cpt} =strrep(Init_RBA.ProcessFull.(idProcess).name,'_',' ');   
    Mat_aa(cpt,1) = valY(i)*Init_RBA.ProcessFull.(idProcess).MM.aa_tot; 
    cpt = cpt+1;
    
end



% Remove unecessary fields if exist
pp = find(strcmpi(labely,'maintenance')>0);
kk = find(strcmpi(labely,'biomass reaction')>0);
ll = find(strcmpi(labely,'Nucleus transport')>0);
labely([pp kk ll]) = []; 
Mat_aa([pp kk ll]) = [];

clear pp kk ll

% Merging processes 
labely_final = labely;
Mat_aa_final = Mat_aa;

% Merged transcription
pp = find(strcmpi(labely_final,'Transcription nucleus RNAPol1')>0);
kk = find(strcmpi(labely_final,'Transcription nucleus RNAPol2')>0);
ll = find(strcmpi(labely_final,'Transcription nucleus RNAPol3')>0);
labely_final([pp kk ll]) = [];  % Remove labels
val_fus = sum(Mat_aa_final([pp kk ll]));
Mat_aa_final([pp kk ll]) = []; % Remove values
labely_final = {labely_final{:},'Transcription nucleus'}; % on retire le nouveau labels
Mat_aa_final = [Mat_aa_final; val_fus];
clear pp kk ll val_fus

% Merged NPC
pp = find(strcmpi(labely_final,'NPC translocation rna')>0);
kk = find(strcmpi(labely_final,'NPC translocation protein')>0);
labely_final([pp kk]) = []; % Remove labels
val_fus = sum(Mat_aa_final([pp kk]));
Mat_aa_final([pp kk]) = []; % Remove values
labely_final = {labely_final{:},'Trafficking nucleus'}; % on retire le nouveau labels
Mat_aa_final = [Mat_aa_final; val_fus];
clear pp kk  val_fus

% Merged TIM/TOM
pp = find(strcmpi(labely_final,'Oxa1 translocation')>0);
kk = find(strcmpi(labely_final,'SAMTIMcx translocation')>0);
ll = find(strcmpi(labely_final,'TIM1723 motor translocation')>0);
uu = find(strcmpi(labely_final,'TIM1723 sort translocation')>0);
jj = find(strcmpi(labely_final,'TIM22 translocation')>0);
bb = find(strcmpi(labely_final,'TOM translocation')>0);
labely_final([pp kk ll uu jj bb]) = []; % Remove labels
val_fus = sum(Mat_aa_final([pp kk ll uu jj bb]));
Mat_aa_final([pp kk ll uu jj bb]) = []; % Remove values
labely_final = {labely_final{:},'Mitochondrial protein import (TIM/TOM)'}; % on retire le nouveau labels
Mat_aa_final = [Mat_aa_final; val_fus];
clear pp kk ll uu jj bb  val_fus


% Merged TIC/TOC
pp = find(strcmpi(labely_final,'Tic Toc photosynthesisP')>0);
kk = find(strcmpi(labely_final,'Tic Toc otherP')>0);
labely_final([pp kk]) = []; % Remove labels
val_fus = sum(Mat_aa_final([pp kk]));
Mat_aa_final([pp kk]) = []; % Remove values
labely_final = {labely_final{:},'Chloroplastic protein import (TIC/TOC)'}; % on retire le nouveau labels
Mat_aa_final = [Mat_aa_final; val_fus];
clear pp kk  val_fus

% Merged chaperones cytoplasm
pp = find(strcmpi(labely_final,'Folding CCT cytoplasm')>0);
kk = find(strcmpi(labely_final,'Folding Hsp70 cytoplasm')>0);
labely_final([pp kk]) = []; % Remove labels
val_fus = sum(Mat_aa_final([pp kk]));
Mat_aa_final([pp kk]) = []; % Remove values
labely_final = {labely_final{:},'Folding CCT/Hsp70 cytoplasm'}; % on retire le nouveau labels
Mat_aa_final = [Mat_aa_final; val_fus];
clear pp kk  val_fus

% Merged chaperones plastid
pp = find(strcmpi(labely_final,'Folding Hsp70 plastid')>0);
kk = find(strcmpi(labely_final,'Folding Hsp6010 plastid')>0);
ll = find(strcmpi(labely_final,'Folding Rubisco')>0);
labely_final([pp kk ll]) = []; % Remove labels
val_fus = sum(Mat_aa_final([pp kk ll]));
Mat_aa_final([pp kk ll]) = []; % Remove values
labely_final = {labely_final{:},'Folding Hsp70/Hsp6010/Rubisco plastid'}; % on retire le nouveau labels
Mat_aa_final = [Mat_aa_final; val_fus];
clear pp kk ll val_fus

%  Pg
labely= {labely{:}, 'House keeping protein (Pg)'};
Mat_aa = [Mat_aa; RBAopt.Pg_tot.aa_tot];
labely_final= {labely_final{:}, 'House keeping protein (Pg)'};
Mat_aa_final = [Mat_aa_final; RBAopt.Pg_tot.aa_tot];

