function trace_sensibility(TabVal,label,Variable,seuil,option)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Research software: PlantCellRBA
% @ Copyright (c) INRAE
% Author: Anne Goelzer (INRAE), anne.goelzer@inrae.fr
% LICENCE: EUPL v1.2 (https://eupl.eu/1.2/en/)
% Requires: MATLAB and IBM ILOG CPLEX (v12.6 or v12.8) licences for code running
% Creation and last modification: 12 May 2023
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
WidthBar = option.WidthBar;
FontSize = option.FontSize;

disp(['Parameters leading to ' Variable ' variation greater than ' num2str(seuil) '%'])
ii = find(abs(TabVal(1,:))>seuil); 
jj = find(abs(TabVal(2,:))>seuil);
kk = unique([ii jj]);
disp(['Total number of parameters: ' num2str(length(kk))])
char(label(kk)')

TabVal_1pcent = TabVal(:,kk);
label_1pcent = label(kk);
[plop,mm] = sort(abs(TabVal_1pcent(1,:)));
TabVal_1pcent_sort = TabVal_1pcent(:,mm);
label_1pcent_sort = label_1pcent(mm);
n_path_trace = size(TabVal_1pcent_sort,2);


TabVal_1pcent_sort_dec = TabVal_1pcent_sort(1,:); % 2-times decreased
TabVal_1pcent_sort_inc = TabVal_1pcent_sort(2,:); % 2-times increased


%--------------------
% create the file of results 
%---------------------

fp = fopen(['simulation/Sensitivity/sensitivity_top_param_' Variable '.csv'],'w');
fprintf(fp,'%s;%s;%s\n','Parameter','two-times decreased','two-times increased');
for i=length(kk):-1:1
    fprintf(fp,'%s;%f;%f\n',label_1pcent_sort{i},TabVal_1pcent_sort_dec(i),TabVal_1pcent_sort_inc(i));
end
fclose(fp);


ind_trace_dec_pos = find(TabVal_1pcent_sort_dec>0);
ind_trace_dec_neg = find(TabVal_1pcent_sort_dec<0);
ind_trace_inc_pos = find(TabVal_1pcent_sort_inc>0);
ind_trace_inc_neg = find(TabVal_1pcent_sort_inc<0);

figure
% parametres 2-times increased, effect +
if ~isempty(ind_trace_inc_pos)
    hh_g=barh(ind_trace_inc_pos,abs(TabVal_1pcent_sort_inc(ind_trace_inc_pos)),WidthBar,'facecolor', [0 0.7 0]);
end
hold on
% parametres 2-times increased, effect -
if ~isempty(ind_trace_inc_neg)
    hh_g=barh(ind_trace_inc_neg,abs(TabVal_1pcent_sort_inc(ind_trace_inc_neg)),WidthBar,'facecolor', 'r');
end
% parametres 2-times decreased, effect +
if ~isempty(ind_trace_dec_pos)
    hh_g=barh(ind_trace_dec_pos,-abs(TabVal_1pcent_sort_dec(ind_trace_dec_pos)),WidthBar,'facecolor', [0 0.7 0]);
end
% parametres 2-times decreased, effect -
if ~isempty(ind_trace_dec_neg)
    hh_g=barh(ind_trace_dec_neg,-abs(TabVal_1pcent_sort_dec(ind_trace_dec_neg)),WidthBar,'facecolor', 'r');
end

set(gca,'YTick',[1:n_path_trace],'YTickLabel',label_1pcent_sort,'YLim', [0 n_path_trace+1],'FontSize',FontSize);
xlabel(['Relative variation of predicted ' Variable ' (%) when the parameter is two-times increased (right) or decreased (left)']);
xtick = num2cell(abs(get(gca,'XTick')));
xticklabel = cellfun(@(x)num2str(x),xtick,'UniformOutput',0);
set(gca,'XTickLabel',xticklabel);
grid on





