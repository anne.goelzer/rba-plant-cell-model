%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Research software: PlantCellRBA
% @ Copyright (c) INRAE
% Author: Anne Goelzer (INRAE), anne.goelzer@inrae.fr
% LICENCE: EUPL v1.2 (https://eupl.eu/1.2/en/)
% Requires: MATLAB and IBM ILOG CPLEX (v12.6 or v12.8) licences for code running
% Creation and last modification: 12 May 2023
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

load simulation/Nominal/Nominal_non_limiting_condition.mat
load simulation/Sensitivity/Sensitivity_enzyme_parameter.mat
mu_var_fus = mu_var;
X_var_fus = X_var;
name_param_rba = name_param_enz;



labely_fus = name_param_rba{1}; 
labely_fus = strrep(labely_fus,'R_PSI1_h_enzyme','R_PSI_h_enzyme');
labely_fus = strrep(labely_fus,'_enzyme','');
labely_fus = strrep(labely_fus,'_','-');




mu_original = RBAopt.mu_opt;
pp = max(strcmp('nu_R_Im_CO2',RBAopt.name_Xv2).*[1:length(RBAopt.name_Xv2)]);
gg = max(strcmp('nu_R_Im_NO3',RBAopt.name_Xv2).*[1:length(RBAopt.name_Xv2)]);
hh = max(strcmp('nu_R_Im_NH4',RBAopt.name_Xv2).*[1:length(RBAopt.name_Xv2)]);

A_original = RBAopt.X2(pp);
N_original = RBAopt.X2(gg)+RBAopt.X2(hh);
ratio_CN_original = abs(A_original/N_original);
%--------------------------------------------------------------------------------------
mu_diff = 100*(mu_var_fus-mu_original)/mu_original;
A_diff = 100*[X_var_fus{1}(pp,:)-A_original; X_var_fus{2}(pp,:)-A_original]/A_original;
ratio_CO_diff = 100*[X_var_fus{1}(pp,:)./(X_var_fus{1}(gg,:)+X_var_fus{1}(hh,:))-ratio_CN_original; ...
    X_var_fus{2}(pp,:)./(X_var_fus{2}(gg,:)+X_var_fus{2}(hh,:))-ratio_CN_original]/ratio_CN_original;




WidthBar = 0.7;
FontSize = 16;
option.WidthBar = WidthBar;
option.FontSize = FontSize;

seuil = 0.4; % 0.4%
%-------------------------------------------------------------
% plot parameters leading to variations greater than 0.4%
%-------------------------------
trace_sensibility(mu_diff,labely_fus,'growth rate',seuil,option);
trace_sensibility(A_diff,labely_fus,'CO2 assimilation rate',seuil,option);
trace_sensibility(ratio_CO_diff,labely_fus,'C over N ratio',seuil,option);
