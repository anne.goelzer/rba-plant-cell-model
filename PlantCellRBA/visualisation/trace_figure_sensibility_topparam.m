%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Research software: PlantCellRBA
% @ Copyright (c) INRAE
% Author: Anne Goelzer (INRAE), anne.goelzer@inrae.fr
% LICENCE: EUPL v1.2 (https://eupl.eu/1.2/en/)
% Requires: MATLAB and IBM ILOG CPLEX (v12.6 or v12.8) licences for code running
% Creation and last modification: 12 May 2023
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
load simulation/Nominal/Nominal_non_limiting_condition.mat
load simulation/Sensitivity/Sensitivity_cell_parameter.mat
mu_var_param = mu_var;
X_var_param = X_var;
name_param_rba = name_param;


load simulation/Sensitivity/Sensitivity_rubisco_parameter.mat
mu_var_rubisco = mu_var;
X_var_rubisco = X_var;

mu_var_fus = [mu_var_param mu_var_rubisco];
X_var_fus{1} = [X_var_param{1} X_var_rubisco{1}];
X_var_fus{2} = [X_var_param{2} X_var_rubisco{2}];
label_p_rubisco = strcat({'Rubisco_'},name_param_rubisco);
labely_fus = [name_param{1} label_p_rubisco];
labely_fus = strrep(labely_fus,'fraction_non_enzymatic_protein','fraction_Pg');
labely_fus = strrep(labely_fus,'fraction_protein','fraction_P');
labely_fus = strrep(labely_fus,'_CONSTANT','');
labely_fus = strrep(labely_fus,'_','-');
labely_fus = strrep(labely_fus,'default-efficiency','default-enzyme-efficiency');
labely_fus = strrep(labely_fus,'M-M-DASH-ACP','Malonyl-ACP');
labely_fus = strrep(labely_fus,'M-ACP','ACP');
labely_fus = strrep(labely_fus,'M-cellulose2-c-concentration','Cellulose (Glucose dimer) concentration');
labely_fus = strrep(labely_fus,'Suc-concentration','Sucrose concentration');
labely_fus = strrep(labely_fus,'M-starch2','Starch');
labely_fus = strrep(labely_fus,'amino-acid-concentration','Peptide-bound amino-acid concentration');

mu_original = RBAopt.mu_opt;
pp = max(strcmp('nu_R_Im_CO2',RBAopt.name_Xv2).*[1:length(RBAopt.name_Xv2)]);
gg = max(strcmp('nu_R_Im_NO3',RBAopt.name_Xv2).*[1:length(RBAopt.name_Xv2)]);
hh = max(strcmp('nu_R_Im_NH4',RBAopt.name_Xv2).*[1:length(RBAopt.name_Xv2)]);


A_original = RBAopt.X2(pp);
N_original = RBAopt.X2(gg) + RBAopt.X2(hh);
ratio_CN_original = abs(A_original/N_original);
%--------------------------------------------------------------------------------------
mu_diff = 100*(mu_var_fus-mu_original)/mu_original;
A_diff = 100*[X_var_fus{1}(pp,:)-A_original; X_var_fus{2}(pp,:)-A_original]/A_original;
ratio_CO_diff = 100*[X_var_fus{1}(pp,:)./(X_var_fus{1}(gg,:)+X_var_fus{1}(hh,:))-ratio_CN_original; ...
    X_var_fus{2}(pp,:)./(X_var_fus{2}(gg,:)+X_var_fus{2}(hh,:))-ratio_CN_original]/ratio_CN_original;




WidthBar = 0.7;
FontSize = 10;
option.WidthBar = WidthBar;
option.FontSize = FontSize;
seuil = 1; % 1%
%-------------------------------------------------------------
% plot parameters leading to variations greater than 1%
%-------------------------------
trace_sensibility(mu_diff,labely_fus,'growth rate',seuil,option);
trace_sensibility(A_diff,labely_fus,'CO2 assimilation rate',seuil,option);
trace_sensibility(ratio_CO_diff,labely_fus,'C over N ratio',seuil,option);
