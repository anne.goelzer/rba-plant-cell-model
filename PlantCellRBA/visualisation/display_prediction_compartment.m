function display_prediction_compartment(RBAopt,PhysioPlante)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Research software: PlantCellRBA
% @ Copyright (c) INRAE
% Author: Anne Goelzer (INRAE), anne.goelzer@inrae.fr
% LICENCE: EUPL v1.2 (https://eupl.eu/1.2/en/)
% Requires: MATLAB and IBM ILOG CPLEX (v12.6 or v12.8) licences for code running
% Creation and last modification: 12 May 2023
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    mm = max(strcmp('f_Cell_membrane',RBAopt.name_Xv2).*[1:length(RBAopt.name_Xv2)]);
    hh = max(strcmp('f_Secreted',RBAopt.name_Xv2).*[1:length(RBAopt.name_Xv2)]);
    listeC = fieldnames(RBAopt.Comp);
    nC = length(listeC);
    disp('Protein allocation per compartment (in %) ')
    [char(RBAopt.name_Xv2(mm:end)) blanks(nC)' num2str(RBAopt.X2(mm:end)*100)] 

   % select the most important ones 
   jjj = [1 2 7:19]-1;
   Label_C = RBAopt.name_Xv2(mm+jjj);
   Label_C_ss = cellfun(@(x)strrep(x,'f_',''),Label_C,'UniformOutput',0);
   Label_C_ss2 = cellfun(@(x)strrep(x,'_',' '),Label_C_ss,'UniformOutput',0);
   Label_C_ss3 = cellfun(@(x)strrep(x,'Plastid','Chloroplast'),Label_C_ss2,'UniformOutput',0);
   Label_C_ss3{11} = 'Chloroplast stroma';

    figure(1000)
   pp_g2=bar([1:length(jjj)],RBAopt.X2(mm+jjj)*100,0.3,'facecolor', 'r');
   ylabel('%')
   xticklabels(Label_C_ss3)
   set(gca,'Fontsize',18,'XTickLabelRotation',45);
   title('Compartments occupancy in cells')
    
    id_mem2 = cellfun(@(x)contains(x,'membrane'),listeC,'UniformOutput',0);
    id_mem = find(cell2mat(id_mem2)~=0);
    AAtot_pred = sum(RBAopt.X2(mm:end))*RBAopt.AAtot;
    disp(sprintf('Total protein (mM of AA/gDW): %f ', AAtot_pred))
    disp(sprintf('Fraction of insoluble proteins: %f ', sum(RBAopt.X2(mm-1+id_mem))))
    AA_soluble = (1-sum(RBAopt.X2(mm-1+id_mem))-RBAopt.X2(hh))*AAtot_pred;
    f_AA_soluble = (1-sum(RBAopt.X2(mm-1+id_mem))-RBAopt.X2(hh))*100;
    disp(sprintf('Fraction of soluble proteins (without extracellular proteins), expected (50-60%%): %f ',f_AA_soluble))
    
    % rubisco
    gg =  max(strcmp('Y_R_RBC_h_enzyme',RBAopt.name_Xv2).*[1:length(RBAopt.name_Xv2)]);
    rubi_vs_leafP = RBAopt.X2(gg)*RBAopt.Pm.R_RBC_h_enzyme.MM.aa_tot/AAtot_pred*100;
    rubi_vs_solubleP = RBAopt.X2(gg)*RBAopt.Pm.R_RBC_h_enzyme.MM.aa_tot/AA_soluble*100;
    disp(sprintf('Fraction of rubisco over total leaf protein (expected 23%%): %f ', rubi_vs_leafP))
    disp(sprintf('Fraction of rubisco over total soluble protein (expected 40%%): %f ', rubi_vs_solubleP))
    
    
    % PSII, PSI
    gg =  max(strcmp('Y_R_RBC_h_enzyme',RBAopt.name_Xv2).*[1:length(RBAopt.name_Xv2)]);
    id_psi2 = cellfun(@(x)contains(x,'Y_R_PSII'),RBAopt.name_Xv2,'UniformOutput',0);
    vecPSII = find(cell2mat(id_psi2)~=0); n_psi2 = length(vecPSII);
    id_psi = cellfun(@(x)contains(x,'Y_R_PSI'),RBAopt.name_Xv2,'UniformOutput',0);
    vecPSII_I = find(cell2mat(id_psi)~=0);
    vecPSI = setdiff(vecPSII_I,vecPSII); n_psi1 = length(vecPSI);
    
    
    % chlorophyll content 
    chlA = zeros(1,length(vecPSII)+length(vecPSI));
    chlB = zeros(1,length(vecPSII)+length(vecPSI));
    for i=1:n_psi2
        idE = char(RBAopt.name_Xv2(vecPSII(i))); idE = deblank(idE);
        idE = idE(3:end);
        chlA(i) = abs(RBAopt.Pm.(idE).MM.Met.M_cholphya_h.stoichiometry)*RBAopt.X2(vecPSII(i));
        chlB(i) = abs(RBAopt.Pm.(idE).MM.Met.M_chlb_h.stoichiometry)*RBAopt.X2(vecPSII(i));
    end
    for i=1:n_psi1
        idE = char(RBAopt.name_Xv2(vecPSI(i))); idE = deblank(idE);
        idE = idE(3:end);
        chlA(n_psi2+i) = abs(RBAopt.Pm.(idE).MM.Met.M_cholphya_h.stoichiometry)*RBAopt.X2(vecPSI(i));
        chlB(n_psi2+i) = abs(RBAopt.Pm.(idE).MM.Met.M_chlb_h.stoichiometry)*RBAopt.X2(vecPSI(i));
    end
    
    MW_chlA = 893.509; % g·mol−1
    MW_chlB = 907.492;  % g·mol−1
    gDW_gFW = PhysioPlante.gDW_gFW;
    SLA = PhysioPlante.SLA;
    
   val_chlA_gFW = sum(chlA)/1000*MW_chlA*1000*gDW_gFW;
   val_chlB_gFW = sum(chlB)/1000*MW_chlB*1000*gDW_gFW;
   disp(sprintf('ChlA (mg/gDW): %f ; ChlA (mg/gFW) (expected 1.17 +/- 0.03): %f ', sum(chlA)/1000*MW_chlA*1000, val_chlA_gFW))
   disp(sprintf('ChlB (mg/gDW): %f ; ChlB (mg/gFW) (expected 0.3 +/- 0.01): %f ', sum(chlB)/1000*MW_chlB*1000, val_chlB_gFW))

   
   % Ribosome
   mmmm = max(strcmp('Y_P_TA_c',RBAopt.name_Xv2).*[1:length(RBAopt.name_Xv2)]); R_c =  RBAopt.X2(mmmm);
   llll = max(strcmp('Y_P_TA_m',RBAopt.name_Xv2).*[1:length(RBAopt.name_Xv2)]); R_m =  RBAopt.X2(llll);
   nnnn = max(strcmp('Y_P_TA_h',RBAopt.name_Xv2).*[1:length(RBAopt.name_Xv2)]); R_h =  RBAopt.X2(nnnn);
    disp(sprintf('Total ribosome content (nmol/gFW): %f ; (expected 0.1 nmol/gFW Piques et al 2009):', (R_c+R_m+R_h)*1e6*gDW_gFW))
   disp(sprintf('Cytoplasmic/Chloroplastic ribosome ratio: %f ; (expected 3-fold Piques et al 2009):', R_c/R_h))
   disp(sprintf('Cytoplasmic/Mitochonrial ribosome ratio: %f ; (expected 30-fold Piques et al 2009):', R_c/R_m))

R_pred = [R_c R_m R_h]*1e6*gDW_gFW;
R_meas = [0.1*30/41 0.1*30/41/30 0.1*30/41/3]; % from piques et al. 2009


   figure(1)
   oo_g=bar([0.6 2.6 4.6],[23 40 60],0.3,'facecolor', 'b');
   hold on
   oo_g2=bar([1.4 3.4 5.4],[rubi_vs_leafP rubi_vs_solubleP f_AA_soluble],0.3,'facecolor', 'r');
   ylabel('%')
   xticks([0 1 2 3 4 5 6])
   xticklabels({'','Rubisco vs Total protein','','Rubisco vs Soluble protein','', 'Soluble vs Total protein', ''})
   set(gca,'Fontsize',18,'XLim',[-0.5 6.5],'XTickLabelRotation',45);
   legend({'Measured (Li et al. 2017)','Predicted'},'Location','northwest')
   title('Protein weight')


   figure(2)
   hh_g=bar([0.6 2.6],[1.17 0.3],0.3,'facecolor', 'b');
   hold on
   hh_g2=bar([1.4 3.4 ],[val_chlA_gFW val_chlB_gFW],0.3,'facecolor', 'r');
   plot([0.6 0.6], [1.14 1.2], 'k-','LineWidth',2)
   plot([2.6 2.6], [0.29 0.31], 'k-','LineWidth',2)
   ylabel('mg/gFW')
   xticks([0 1 2 3 4])
   xticklabels({'','ChlA','','ChlB',''})
   set(gca,'Fontsize',18,'XLim',[-0.5 4.5]);
   legend({'Measured (Sulpice et al. 2014)','Predicted'},'Location','northeast')
   title('Chlorophyll content')



   figure(3)
   bar([R_meas; R_pred],'stacked')
   legend({'Cytoplasmic','Mitochondrial','Chloroplastic'},'Location','northwest')
   ylabel('nmol/gFW')
   set(gca,'Fontsize',18)
   xticks([1 2])
   xticklabels({'Measured','Predicted'})
   title('Ribosome content')
   


   
