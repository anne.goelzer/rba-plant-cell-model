%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Research software: PlantCellRBA
% @ Copyright (c) INRAE
% Author: Anne Goelzer (INRAE), anne.goelzer@inrae.fr
% LICENCE: EUPL v1.2 (https://eupl.eu/1.2/en/)
% Requires: MATLAB and IBM ILOG CPLEX (v12.6 or v12.8) licences for code running
% Creation and last modification: 12 May 2023
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

load simulation/Nominal/scenario_original_21deg_nonlimiting_growth_merged_complexes.mat
    
    mm = max(strcmp('f_Cell_membrane',RBAopt.name_Xv2).*[1:length(RBAopt.name_Xv2)]);
    hh = max(strcmp('f_Secreted',RBAopt.name_Xv2).*[1:length(RBAopt.name_Xv2)]);
    listeC = fieldnames(RBAopt.Comp);
    nC = length(listeC);
    disp('Protein allocation per compartment + Lagrangian')
    [char(RBAopt.name_Xv2(mm:end)) blanks(nC)' num2str(RBAopt.X2(mm:end)) blanks(nC)'  num2str(RBAopt.lambda.ineqlin(1:nC))] 
    [char(RBAopt.name_Xv2(mm:end)) blanks(nC)' num2str(RBAopt.X2(mm:end)*100)] 

   f_comp = RBAopt.X2(mm:end)*100;

   % selection of the most important compartments
   jjj = [1 2 7:19]-1;
   Label_C = RBAopt.name_Xv2(mm+jjj);
   Label_C_ss = cellfun(@(x)strrep(x,'f_',''),Label_C,'UniformOutput',0);
   Label_C_ss2 = cellfun(@(x)strrep(x,'_',' '),Label_C_ss,'UniformOutput',0);
   Label_C_ss3 = cellfun(@(x)strrep(x,'Plastid','Chloroplast'),Label_C_ss2,'UniformOutput',0);
   Label_C_ss3{11} = 'Chloroplast stroma';
   
   fp = fopen('data_compartment_occupancy.csv','w');
   fprintf(fp,'%s;%s;\n','Compartment','Protein allocation (%)')
   for i=1:length(Label_C_ss3)
      fprintf(fp,'%s;%d;\n',Label_C_ss3{i},f_comp(jjj(i)+1)) 
   end
   fclose(fp)
   ! mv data_compartment_occupancy.csv data_for_figure2/
   
   
   
    id_mem2 = cellfun(@(x)contains(x,'membrane'),listeC,'UniformOutput',0);
    id_mem = find(cell2mat(id_mem2)~=0);
    AAtot_pred = sum(RBAopt.X2(mm:end))*RBAopt.AAtot;
    disp(sprintf('Total protein: %f ', AAtot_pred))
    disp(sprintf('Fraction of insoluble proteins: %f ', sum(RBAopt.X2(mm-1+id_mem))))
    AA_soluble = (1-sum(RBAopt.X2(mm-1+id_mem))-RBAopt.X2(hh))*AAtot_pred;
    f_AA_soluble = (1-sum(RBAopt.X2(mm-1+id_mem))-RBAopt.X2(hh))*100;
    disp(sprintf('Fraction of soluble proteins (without extracellular proteins), expected (50-60%%): %f ',f_AA_soluble))
    
    % rubisco
    gg =  max(strcmp('Y_R_RBC_h_enzyme',RBAopt.name_Xv2).*[1:length(RBAopt.name_Xv2)]);
    rubi_vs_leafP = RBAopt.X2(gg)*RBAopt.Pm.R_RBC_h_enzyme.MM.aa_tot/AAtot_pred*100;
    rubi_vs_solubleP = RBAopt.X2(gg)*RBAopt.Pm.R_RBC_h_enzyme.MM.aa_tot/AA_soluble*100;
    disp(sprintf('Fraction of rubisco over total leaf protein (expected 23%%): %f ', rubi_vs_leafP))
    disp(sprintf('Fraction of rubisco over total soluble protein (expected 40%%): %f ', rubi_vs_solubleP))
   
    fp1 = fopen('data_protein_weight.csv','w');
    fprintf(fp1,'%s;%s;%s;\n','Case','Predicted (%)','Measured (%)')
    fprintf(fp1,'%s;%d;%s;\n','Fraction of soluble proteins (without extracellular proteins) wrt total protein',f_AA_soluble,'50-60')
    fprintf(fp1,'%s;%d;%d;\n','Fraction of rubisco over total leaf protein',rubi_vs_leafP,23)
    fprintf(fp1,'%s;%d;%d;\n','Fraction of rubisco over total soluble protein' ,rubi_vs_solubleP,40)
    fclose(fp1)
   ! mv data_protein_weight.csv data_for_figure2/
   
   
    % PSII, PSI
    gg =  max(strcmp('Y_R_RBC_h_enzyme',RBAopt.name_Xv2).*[1:length(RBAopt.name_Xv2)]);
    id_psi2 = cellfun(@(x)contains(x,'Y_R_PSII'),RBAopt.name_Xv2,'UniformOutput',0);
    vecPSII = find(cell2mat(id_psi2)~=0); n_psi2 = length(vecPSII);
    id_psi = cellfun(@(x)contains(x,'Y_R_PSI'),RBAopt.name_Xv2,'UniformOutput',0);
    vecPSII_I = find(cell2mat(id_psi)~=0);
    vecPSI = setdiff(vecPSII_I,vecPSII); n_psi1 = length(vecPSI);
   
   
    %  chlorophyll content
    chlA = zeros(1,length(vecPSII)+length(vecPSI));
    chlB = zeros(1,length(vecPSII)+length(vecPSI));
    for i=1:n_psi2
        idE = char(RBAopt.name_Xv2(vecPSII(i))); idE = deblank(idE);
        idE = idE(3:end);
        chlA(i) = abs(RBAopt.Pm.(idE).MM.Met.M_cholphya_h.stoichiometry)*RBAopt.X2(vecPSII(i));
        chlB(i) = abs(RBAopt.Pm.(idE).MM.Met.M_chlb_h.stoichiometry)*RBAopt.X2(vecPSII(i));
    end
    for i=1:n_psi1
        idE = char(RBAopt.name_Xv2(vecPSI(i))); idE = deblank(idE);
        idE = idE(3:end);
        chlA(n_psi2+i) = abs(RBAopt.Pm.(idE).MM.Met.M_cholphya_h.stoichiometry)*RBAopt.X2(vecPSI(i));
        chlB(n_psi2+i) = abs(RBAopt.Pm.(idE).MM.Met.M_chlb_h.stoichiometry)*RBAopt.X2(vecPSI(i));
    end
    
    MW_chlA = 893.509; % g·mol−1
    MW_chlB = 907.492;  % g·mol−1
    gDW_gFW = PhysioPlante.gDW_gFW;
    SLA = PhysioPlante.SLA;
    
   val_chlA_gFW = sum(chlA)/1000*MW_chlA*1000*gDW_gFW;
   val_chlB_gFW = sum(chlB)/1000*MW_chlB*1000*gDW_gFW;
   disp(sprintf('ChlA (mg/gDW): %f ; ChlA (mg/gFW) (expected 1.17 +/- 0.03): %f ', sum(chlA)/1000*MW_chlA*1000, val_chlA_gFW))
   disp(sprintf('ChlB (mg/gDW): %f ; ChlB (mg/gFW) (expected 0.3 +/- 0.01): %f ', sum(chlB)/1000*MW_chlB*1000, val_chlB_gFW))

    fp2 = fopen('data_chlorophyll.csv','w');
    fprintf(fp2,'%s;%s;%s;\n','Chloropyll','Predicted (mg/gFW)','Measured (mg/gFW)')
    fprintf(fp2,'%s;%d;%s;\n','Chlorophyll A',val_chlA_gFW,'1.17 +/- 0.03')
    fprintf(fp2,'%s;%d;%s;\n','Chlorophyll B',val_chlB_gFW,'0.3 +/- 0.01')
    fclose(fp2)
   ! mv data_chlorophyll.csv data_for_figure2/
   
   
      mmmm = max(strcmp('Y_P_TA_c',RBAopt.name_Xv2).*[1:length(RBAopt.name_Xv2)]); R_c =  RBAopt.X2(mmmm);
   llll = max(strcmp('Y_P_TA_m',RBAopt.name_Xv2).*[1:length(RBAopt.name_Xv2)]); R_m =  RBAopt.X2(llll);
   nnnn = max(strcmp('Y_P_TA_h',RBAopt.name_Xv2).*[1:length(RBAopt.name_Xv2)]); R_h =  RBAopt.X2(nnnn);
    disp(sprintf('Total ribosome content (nmol/gFW): %f ; (expected 0.1 nmol/gFW Piques et al 2009):', (R_c+R_m+R_h)*1e6*gDW_gFW))
   disp(sprintf('Cytoplasmic/Chloroplastic ribosome ratio: %f ; (expected 3-fold Piques et al 2009):', R_c/R_h))
   disp(sprintf('Cytoplasmic/Mitochonrial ribosome ratio: %f ; (expected 30-fold Piques et al 2009):', R_c/R_m))

R_pred = [R_c R_m R_h]*1e6*gDW_gFW;
R_meas = [0.1*30/41 0.1*30/41/30 0.1*30/41/3]; 
   
       fp3 = fopen('data_ribosome.csv','w');
    fprintf(fp3,'%s;%s;%s;\n','Ribosome','Predicted','Measured')
    fprintf(fp3,'%s;%d;%d;\n','Total ribosome content (nmol/gFW)',(R_c+R_m+R_h)*1e6*gDW_gFW,0.1)
    fprintf(fp3,'%s;%d;%d;\n','Cytoplasmic ribosome (nmol/gFW)',R_c*1e6*gDW_gFW,0.0727)
    fprintf(fp3,'%s;%d;%d;\n','Mitochondrial ribosome (nmol/gFW)',R_m*1e6*gDW_gFW,0.0022) 
    fprintf(fp3,'%s;%d;%d;\n','Chloroplastic ribosome (nmol/gFW)',R_h*1e6*gDW_gFW,0.0257)     
    fprintf(fp3,'%s;%d;%s;\n','Cytoplasmic/Mitochonrial ribosome ratio',R_c/R_m,'30')
    fprintf(fp3,'%s;%d;%s;\n','Cytoplasmic/Chloroplastic ribosome ratio',R_c/R_h,'3')
    fclose(fp3)
   ! mv data_ribosome.csv data_for_figure2/
   
   
     id_in = cellfun(@(x)startsWith(x,'nu_R_Im_'),RBAopt.name_Xv2,'UniformOutput',0);
    vecIn = find(cell2mat(id_in)~=0);
    id_out = cellfun(@(x)startsWith(x,'nu_R_Ex_'),RBAopt.name_Xv2,'UniformOutput',0);
    vecOut = find(cell2mat(id_out)~=0);
    
    
    mm = max(strcmp('nu_R_Si_H',RBAopt.name_Xv2).*[1:length(RBAopt.name_Xv2)]);
    pp = max(strcmp('nu_R_Im_CO2',RBAopt.name_Xv2).*[1:length(RBAopt.name_Xv2)]);
    kk = max(strcmp('nu_R_StS_h2',RBAopt.name_Xv2).*[1:length(RBAopt.name_Xv2)]);
    
    
    disp(sprintf('Flux of H+ (mM/gDW/d): %f ', RBAopt.X2(mm)))
    
    disp('Flux imported (mM/gDW/d)')
    [char(RBAopt.name_Xv2(vecIn)) blanks(length(vecIn))' num2str(RBAopt.X2(vecIn)) ] 
    
     disp('Flux exported (mM/gDW/d)')
    [char(RBAopt.name_Xv2(vecOut)) blanks(length(vecOut))' num2str(RBAopt.X2(vecOut)) ] 
    
fp5 = fopen('flux_in_out.csv','w');
   fprintf(fp5,'%s;%s;\n','Reaction','Flux value (mM/gDW/day)')
   for i=1:length(vecIn)
      fprintf(fp5,'%s;%d;\n',RBAopt.name_Xv2{vecIn(i)},RBAopt.X2(vecIn(i))) 
   end
   for i=1:length(vecOut)
       fprintf(fp5,'%s;%d;\n',RBAopt.name_Xv2{vecOut(i)},RBAopt.X2(vecOut(i)))
   end
   fclose(fp5)
   ! mv flux_in_out.csv data_for_figure2/
   
