%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% script performing the global sensitibity analysis of the RBA
% model of the plant cell (Supplementary figure 7)
% -> joint variation of molecular machine efficiencies
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Research software: PlantCellRBA
% @ Copyright (c) INRAE
% Author: Anne Goelzer (INRAE), anne.goelzer@inrae.fr
% LICENCE: EUPL v1.2 (https://eupl.eu/1.2/en/)
% Requires: MATLAB and IBM ILOG CPLEX (v12.6 or v12.8) licences for code running
% Creation and last modification: 12 May 2023
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% This line needs to be updated with the path to CPLEX
% addpath My/path/to/Cplex

addpath ./visualisation/
addpath ./utils/
addpath ./RBA_xml/
addpath ./RBA_mat/

clear all;
close all;



disp('---- Init RBA ----')
% load prot, RNA, DNA
load RBA_mat/Prot_RNA_DNA_forRBA
    
% load Pm Process
load RBA_mat/mol_machines
    
% load metabolism: compartments, Reac, Metab S
load RBA_mat/metabolism
    
% load targets:TargetsMF TargetDensities
load RBA_mat/targets
    
%  load Parameters: Parameters ParameterFunctions
load RBA_mat/parameters

%----------------------------------------------------    

% Report functional annotation 
[Reac,Subsystem,Subsystem_curated] = report_functional_annotation_from_FBA(Reac,'RBA_mat/AraCore.mat');

% Values from Suplice et al. 2014
% Table supp 2 for 18.97mg
PhysioPlante.gDW_gFW = 8.35/100; % fraction of DW in FW
PhysioPlante.protein_content_mg_FW = 18.97; % mg/gFW
PhysioPlante.SLA = 31.6/1000; % specific leaf area. m2/kgDW converted in m2/gDW in Pyl et al. 2012
PhysioPlante.day_length = 8; % hours


% Initalisation of the medium
% Medium: les id sont importants
% ce sont eux qui font la correspondance avec les parametres
% dans les fonctions de michelis menten
Medium = import_medium('e');
Medium.M_CO2_e = 200e-3/1.01325; % 50 to 400 umol
Medium.M_O2_e = 210/1.01325;
Medium.M_hnu_e = 1000; %umol
Medium.M_Suc_e =0;
Temperature_celsius = 21;

% On update la structure fonction avec les valeurs des param du milieu
ParameterFunctions_m = update_function_medium(ParameterFunctions,Medium);

% Parameters ParameterFunctions
Pm = update_max_efficiency(Pm,Parameters,ParameterFunctions_m);

% on update les parametres de la rubisco avec le milieu exterieur
Pm = update_efficiency_rubisco(Pm,Medium.M_CO2_e,Medium.M_O2_e,Medium.M_hnu_e,Temperature_celsius);


% remove the boundary metabolite from the stoichioetry matrix S
S_full = S;
S(id_ex,:) = [];

% on update la structure
MetabFull = Metab;
clear Metab

listeMetabFull = fieldnames(MetabFull);
NMetFull = length(listeMetabFull);
cpt = 1;
for i=1:NMetFull
    % test si machine non nulle
    if MetabFull.(listeMetabFull{i}).boundaryCondition == 0
        Metab.(listeMetabFull{i}) = MetabFull.(listeMetabFull{i});
        Metab.(listeMetabFull{i}).number_S = cpt;
        cpt = cpt+1;
    end
end




%-----------------
% Init struc
Init_RBA.Pm = Pm;
Init_RBA.ProcessFull = ProcessFull;
Init_RBA.ListeAllElementMachine = ListeAllElementMachine;
Init_RBA.Medium = Medium;
Init_RBA.S_full = S_full;
Init_RBA.S = S;
Init_RBA.status_nu = status_nu;
Init_RBA.id_met_boundary = id_ex;
Init_RBA.Comp = Comp;
Init_RBA.Metab = Metab;
Init_RBA.Reac = Reac;
Init_RBA.RNA = RNA;
Init_RBA.DNA = DNA;
Init_RBA.ParameterFunctions_m = ParameterFunctions_m;
Init_RBA.Parameters = Parameters;
Init_RBA.TargetsMF = TargetsMF;
Init_RBA.TargetsDensities = TargetDensities;
Init_RBA.TargetMC = TargetMC;
Init_RBA.TargetMetab = TargetMetab;
Init_RBA.IdTargetReac = IdTargetReac;
Init_RBA.IdAllTargetSpecies = IdAllSpecies;
Init_RBA.ProcessingMaps = ProcessingMaps;
Init_RBA.Subsystem = Subsystem;
Init_RBA.Subsystem_curated = Subsystem_curated;
Init_RBA.Prot = Prot;
Init_RBA.Temperature_celsius = Temperature_celsius;

% options
optionsRBA.seuil_mu = 1e-4;
optionsRBA.display_cplex = 'off';
optionsRBA.display_iter_mu = 'off';

%-----------------------------
% build RBA model
disp('---- Build model (takes ~1 min) ----')
[RBAmodel] = build_RBAmodel_eukaryotes(Init_RBA);


listePm = fieldnames(RBAmodel.Pm);
listeProcess = fieldnames(RBAmodel.Process);
Pm_original = Init_RBA.Pm;
Pm = Pm_original;
RBAmodel_original = RBAmodel;
RBAmodel_tmp = RBAmodel_original;
Nm = length(listePm);
Np = length(listeProcess);
pd = makedist('Lognormal','mu',log(7),'sigma',0.7);
pd2 = makedist('Lognormal','mu',log(4),'sigma',0.7);
pd3 = makedist('Lognormal','mu',log(3),'sigma',0.7);
pd4 = makedist('Lognormal','mu',log(50),'sigma',0.7);
pd5 = makedist('Lognormal','mu',log(6.3),'sigma',0.7);
pd6 = makedist('Lognormal','mu',log(21),'sigma',0.7);
pd7 = makedist('Lognormal','mu',log(48),'sigma',0.7);
pd8 = makedist('Lognormal','mu',log(3.3),'sigma',0.7);

rng('default');  % For reproducibility
n_boot = 1000;

kE_random = random(pd,Nm,n_boot)*24*3600;
kC_random = random(pd2,length(fieldnames(RBAmodel.Process)),n_boot)*24*3600;
kT_random = random(pd3,1,n_boot)*24*3600;
kRpol1_random = random(pd4,1,n_boot)*24*3600;
kRpol2_random = random(pd5,1,n_boot)*24*3600;
kRpol3_random = random(pd6,1,n_boot)*24*3600;
kDpol_random = random(pd7,1,n_boot)*24*3600;

mu_var = zeros(1,n_boot);
Xvar = zeros(1631,n_boot);
for k=1:n_boot
    disp(['nboot: ' num2str(k)])
    
    % update of Pm
    for i=1:length(listePm)
        if strcmpi(listePm{i},'R_RBC_h_enzyme') || strcmpi(listePm{i},'R_RBC_h_duplicate_2_enzyme') ...
                || strcmpi(listePm{i},'R_RBC_h_duplicate_3_enzyme') || strcmpi(listePm{i},'R_RBC_h_duplicate_4_enzyme') ...
                || strcmpi(listePm{i},'R_RBO_h_enzyme') || strcmpi(listePm{i},'R_RBO_h_duplicate_2_enzyme') ...
                || strcmpi(listePm{i},'R_RBO_h_duplicate_3_enzyme') || strcmpi(listePm{i},'R_RBO_h_duplicate_4_enzyme') ...
                || strcmpi(listePm{i},'R_PSII_h_enzyme') || strcmpi(listePm{i},'R_PSII2_h_enzyme') ...
                || strcmpi(listePm{i},'R_PSII3_h_enzyme') || strcmpi(listePm{i},'R_PSII4_h_enzyme')
            % on modifie tout uniquement comme RBC_enzyme
          
            
            
        else           
            % update eff
            % only if the values are strictly positive
            % otherwise keep 0 (<=> irreversibility)
            if Pm_original.(listePm{i}).keff_f>0
                Pm.(listePm{i}).keff_f = kE_random(i,k);
            end
            if Pm_original.(listePm{i}).keff_b>0
                Pm.(listePm{i}).keff_b = kE_random(i,k);
            end
                
            % et maintenant on update uniquement la matrice A0
            RBAmodel_tmp.matrix.A0(RBAmodel_tmp.Sind.ind_iq_nu+i,RBAmodel_tmp.Sind.offset_concentration+i) = -Pm.(listePm{i}).keff_f;
            RBAmodel_tmp.matrix.A0(RBAmodel_tmp.Sind.ind_iq_nu+Nm+i,RBAmodel_tmp.Sind.offset_concentration+i) = -Pm.(listePm{i}).keff_b;
            RBAmodel_tmp.matrix.A0_mean_complex(RBAmodel_tmp.Sind.ind_iq_nu+i,RBAmodel_tmp.Sind.offset_concentration+i) = -Pm.(listePm{i}).keff_f;
            RBAmodel_tmp.matrix.A0_mean_complex(RBAmodel_tmp.Sind.ind_iq_nu+Nm+i,RBAmodel_tmp.Sind.offset_concentration+i) = -Pm.(listePm{i}).keff_b;                        
            
            Init_RBA.Pm = Pm;

        end
        

        
    end
    % update of Process
    
    disp('---- Look for µ  ----')
    keff_rand.kC_random = kC_random(:,k);
    keff_rand.kT_random = kT_random(k);
    keff_rand.kRpol1_random = kRpol1_random(k);
    keff_rand.kRpol2_random = kRpol2_random(k);
    keff_rand.kRpol3_random = kRpol3_random(k);
    keff_rand.kDpol_random = kDpol_random(k);
    
    [RBAopt] = solve_RBAmodel_eukaryotes_varykeff(RBAmodel_tmp,Init_RBA,optionsRBA,keff_rand);
    
    if ~isempty(RBAopt)
        mu_var(k) = RBAopt.mu_opt;
        Xvar(:,k) = RBAopt.X2;
    end
    
    % on reinitialise ParameterFunctions_m et Pm à sa valeur
    % d'origine: ParameterFunctions_m_original
    Pm = Pm_original;
    RBAmodel_tmp = RBAmodel_original;
    
end

%save simulation/Sensitivity/Sensitivity_random_all_efficiencies.mat u_var Xvar Init_RBA keff_rand n_boot
