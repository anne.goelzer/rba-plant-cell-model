%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% script which build and simulate the RBA model of the plant cell
% 1. initialisation of the composition in proteins, and of the metabolic
% network
% 2. Build the RBA model
% 3. Simulate the RBA model by searching the maximal growth rate compatible 
% with the extracellular resources 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Research software: PlantCellRBA
% @ Copyright (c) INRAE
% Author: Anne Goelzer (INRAE), anne.goelzer@inrae.fr
% LICENCE: EUPL v1.2 (https://eupl.eu/1.2/en/)
% Requires: MATLAB and IBM ILOG CPLEX (v12.6 or v12.8) licences for code running
% Creation and last modification: 12 May 2023
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% This line needs to be updated with the path to CPLEX
% addpath My/path/to/Cplex
addpath ./visualisation/
addpath ./utils/
addpath ./RBA_xml/
addpath ./RBA_mat/

clear all;
close all;

disp('---- Init RBA ----')
% load prot, RNA, DNA
load RBA_mat/Prot_RNA_DNA_forRBA
    
% load Pm Process
load RBA_mat/mol_machines
    
% load metabolism: compartments, Reac, Metab S
load RBA_mat/metabolism
    
% load targets:TargetsMF TargetDensities
load RBA_mat/targets
    
%  load Parameters: Parameters ParameterFunctions
load RBA_mat/parameters

%----------------------------------------------------    

% Report functional annotation 
[Reac,Subsystem,Subsystem_curated] = report_functional_annotation_from_FBA(Reac,'RBA_mat/AraCore.mat');

% Values from Suplice et al. 2014
% Table supp 2 for 18.97mg
% 8h/jour, 16h/nuit, 21°C/19°C
PhysioPlante.gDW_gFW = 8.35/100; % fraction of DW in FW
PhysioPlante.protein_content_mg_FW = 18.97; % mg/gFW
PhysioPlante.SLA = 31.6/1000; % specific leaf area. m2/kgDW converted in m2/gDW in Pyl et al. 2012
PhysioPlante.day_length = 8; % hours

%-----------------------------------------------
% Initialisation of the medium from the file medium.tsv in RBA-xml
% directory
% To change the medium composition
%     - look at the fields of the structure by typing Medium in the workspace
%     - update the corresponding value
% The unit is millimoles
% ex: Medium.M_Frc_e = 10 will set the concentration of extracellular
% fructose to 10mM
%
% Fields are as follows:
% M_k_e (K+), M_O2_e (O2), M_ni2_e (Ni2+), M_Lys_e (Lysine), M_cl_e (Cl-)
% M_SO4_e (Sulfate), M_Frc_e (Fructose), M_His_e (Histidine), M_cu_e (Cu+)
% M_mn2_e (Mn2+), M_cobalt2_e (Co2+), M_fe2_e (Fe2+), M_Mas_e (Maltose), 
% M_CO2_e (CO2), M_NH4_e (amonium), M_H2S_e (H2S), M_Ala_e (Alanine), 
% M_H_e (H+), M_Trp_e (Tryptophan), M_Leu_e (Leucine), M_Asp_e (Aspartate)
% M_Met_e (Methionine), M_Tre_e (Trehalose), M_Asn_e (Asparagine)
% M_Phe_e (Phenylalanine), M_ca2_e (Ca2+), M_Val_e (Valine), M_hnu_e (Photon)
% M_H2O_e (Water), M_zn2_e (Zn+), M_NO3_e (Nitrate), M_Pi_e (Orthophosphate)
% M_Glc_e (Glucose), M_Gln_e (Glutamine), M_Ser_e (Serine), M_Ile_e (Isoleucine)
% M_mg2_e (Mg2+), M_Tyr_e (Tyrosine), M_Glu_e (Glutamate),  M_Cys_e (Cysteine)
% M_Gly_e (Glycine), M_Thr_e (Threonine), M_Pro_e (Proline), M_Suc_e (Sucrose)
% M_cd2_e (Cd2+), M_Arg_e (Arginine)

%--------------------------------------------
% pression atmospherique: 1.01325 bar
Medium = import_medium('e');
Medium.M_CO2_e = 200e-3/1.01325; % 50 to 400 umol
Medium.M_O2_e = 210/1.01325;
Medium.M_hnu_e = 100; 
Medium.M_Suc_e =0;
Temperature_celsius = 21;


% update the structure functions with medium
ParameterFunctions_m = update_function_medium(ParameterFunctions,Medium);

% Update transporter efficiencies with medium
Pm = update_max_efficiency(Pm,Parameters,ParameterFunctions_m);

% Update Rubisco efficiency with medium 
Pm = update_efficiency_rubisco(Pm,Medium.M_CO2_e,Medium.M_O2_e,Medium.M_hnu_e,Temperature_celsius);


% remove extracellular metabolites
S_full = S;
S(id_ex,:) = [];

% on update la structure
MetabFull = Metab;
clear Metab

listeMetabFull = fieldnames(MetabFull);
NMetFull = length(listeMetabFull);
cpt = 1;
for i=1:NMetFull
    % test si machine non nulle
    if MetabFull.(listeMetabFull{i}).boundaryCondition == 0
        Metab.(listeMetabFull{i}) = MetabFull.(listeMetabFull{i});
        Metab.(listeMetabFull{i}).number_S = cpt;
        cpt = cpt+1;
    end
end



%-----------------
% Init struc
Init_RBA.Pm = Pm;
Init_RBA.ProcessFull = ProcessFull;
Init_RBA.ListeAllElementMachine = ListeAllElementMachine;
Init_RBA.Medium = Medium;
Init_RBA.S_full = S_full;
Init_RBA.S = S;
Init_RBA.status_nu = status_nu;
Init_RBA.id_met_boundary = id_ex;
Init_RBA.Comp = Comp;
Init_RBA.Metab = Metab;
Init_RBA.MetabFull = MetabFull;
Init_RBA.Reac = Reac;
Init_RBA.RNA = RNA;
Init_RBA.DNA = DNA;
Init_RBA.ParameterFunctions_m = ParameterFunctions_m;
Init_RBA.Parameters = Parameters;
Init_RBA.TargetsMF = TargetsMF;
Init_RBA.TargetsDensities = TargetDensities;
Init_RBA.TargetMC = TargetMC;
Init_RBA.TargetMetab = TargetMetab;
Init_RBA.IdTargetReac = IdTargetReac;
Init_RBA.IdAllTargetSpecies = IdAllSpecies;
Init_RBA.ProcessingMaps = ProcessingMaps;
Init_RBA.Subsystem = Subsystem;
Init_RBA.Subsystem_curated = Subsystem_curated;
Init_RBA.Prot = Prot;
Init_RBA.Temperature_celsius = Temperature_celsius;

% option of simulation
optionsRBA.seuil_mu = 1e-6;
optionsRBA.display_cplex = 'off';
optionsRBA.display_iter_mu = 'on';


%-----------------------------
% build + solve the pb rba
disp('---- Build model (takes ~1min)  ----')
[RBAmodel] = build_RBAmodel_eukaryotes(Init_RBA);

disp('---- Look for µ  ----')
[RBAopt] = solve_RBAmodel_eukaryotes(RBAmodel,Init_RBA,optionsRBA);


if ~isempty(RBAopt)
    % Uncomment this line if you want the flux distribution displayed in
    % the workspace (Flux distribution is also generated in a file below
    
    % display_flux(RBAopt)
    display_prediction_compartment(RBAopt,PhysioPlante);
    display_flux_in_out_cell(RBAopt,PhysioPlante);
    [labely,Mat_aa] = compute_repartition_prot_path(RBAopt,Init_RBA);
     FaceColor = 'b'; %  b, r, k, y, g, c, m 
     WidthBar = 0.7;% width of bar: from 0.5 to 1
     FontSize = 9;
     % plot the mass repartition wrt cellular function
    trace_repartition_prot_path(labely,Mat_aa, FaceColor, WidthBar,FontSize);
    % write the flux distribution in Flux_distribution.csv
    write_result(RBAopt,Init_RBA);
end


% save simulation/Nominal/Nominal_non_limiting_condition  Init_RBA RBAopt RBAmodel PhysioPlante optionsRBA
