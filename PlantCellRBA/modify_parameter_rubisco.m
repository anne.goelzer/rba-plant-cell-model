function Pm = modify_parameter_rubisco(Pm,CO2,O2,hnu,T_celcius,name_param,f_mul)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Research software: PlantCellRBA
% @ Copyright (c) INRAE
% Author: Anne Goelzer (INRAE), anne.goelzer@inrae.fr
% LICENCE: EUPL v1.2 (https://eupl.eu/1.2/en/)
% Requires: MATLAB and IBM ILOG CPLEX (v12.6 or v12.8) licences for code running
% Creation and last modification: 12 May 2023
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Values for kcat, Kc, Km at 25°C were extracted 
% from  walker et al. Plant cell env, 2014.
T_kelvin = T_celcius + 273.15;
R = 8.31; % J/K/mol


T_sat_rubisco = 310; % kelvin

%------------------------------------------------------------
% Initialise parameters 

% From Biochemical Model of C 3 Photosynthesis
% von caemmer et al. 
E_Ko = 35.94e3; %J/mol
E_Kc = 59.36e3; %J/mol
E_Vcmax = 58.52e3; % J/mol

% from table 5 walker et al. Plant cell env, 2014.
Kc_25 = 312e-3; % mbar
Ko_25 =  181; % mbar
ratio_Vomax_Vc_max_25 = 0.24;

% from table 3 walker et al. Plant cell env, 2014.
kcat_c_25 = 3.3*3600*24;

% ratio Jmax/Vcmax
% compute from Table  4 of walker et al. Plant cell env, 2014.
% normallybetween 1.5 et 2 à 25°C. 
% 2.44 ds walker et al .
ratio_Jmax25_Vcmax25 = 2.4441;
E_Jmax = 37e3; % J/mol
H = 220e3; %J/mol
S = 710; % J/K/mol

%----------------------------------------------------------------
% Modify with the multiplicative factor f_mul

switch name_param
    
    case 'kcat_c_25'
        kcat_c_25 = kcat_c_25*f_mul;
        
    case 'Kc_25' 
        Kc_25 = Kc_25*f_mul;
                
    case 'Ko_25' 
        Ko_25 = Ko_25*f_mul;
        
    case 'ratio_Vomax_Vc_max_25'
        ratio_Vomax_Vc_max_25 = ratio_Vomax_Vc_max_25*f_mul;
        
    case 'ratio_Jmax25_Vcmax25' 
        ratio_Jmax25_Vcmax25 = ratio_Jmax25_Vcmax25*f_mul;
        
    case  'E_Ko'
       E_Ko =  E_Ko*f_mul;
       
    case 'E_Kc' 
       E_Kc =  E_Kc*f_mul;
       
    case 'E_Vcmax' 
        E_Vcmax = E_Vcmax*f_mul;
         
    case 'E_Jmax' 
        E_Jmax = E_Jmax*f_mul;
        
    case  'H' 
        H = H*f_mul;
        
    case 'S'
        S = S*f_mul;
        
    case 'T_sat_rubisco'
        T_sat_rubisco = T_sat_rubisco*f_mul;    
        
    otherwise
        disp('Unknown parameter')
end

% activation rubisco
aR = 1/(1+exp(0.3*(T_kelvin-T_sat_rubisco))); % activation rubisco

% compute efficiencies 
kcat_o_25 = kcat_c_25*ratio_Vomax_Vc_max_25;

% compute kcat_c, kcat_o, Kc, Ko 
kcat_c =aR*kcat_c_25*exp((T_celcius-25)*E_Vcmax/(298*R*T_kelvin));% 4.1 1/s
kcat_o = aR*kcat_o_25*exp((T_celcius-25)*E_Vcmax/(298*R*T_kelvin));% 1 1/s
Kc = Kc_25/1.01325*exp((T_celcius-25)*E_Kc/(298*R*T_kelvin)); % 9.8µM converti en mM
Ko = Ko_25/1.01325*exp((T_celcius-25)*E_Ko/(298*R*T_kelvin)); % 470µM


kcat_psiII_25 = kcat_c_25*ratio_Jmax25_Vcmax25*(hnu/(1+hnu)); 
keff_psiII = kcat_psiII_25*exp((T_kelvin-298)*E_Jmax/(298*R*T_kelvin))*(1+exp((298*S-H)/298/R))/(1+exp((S*T_kelvin-H)/R/T_kelvin));

keff_rubisco_carboxylase = kcat_c/(1+ Kc/CO2 + Kc*O2/Ko/CO2);
keff_rubisco_oxygenase = kcat_o/(1 + Ko/O2 +  Ko*CO2/Kc/O2);

phi = kcat_o*O2*Kc/kcat_c/CO2/Ko;


Pm.R_RBC_h_enzyme.keff_f = keff_rubisco_carboxylase;
Pm.R_RBC_h_duplicate_2_enzyme.keff_f = keff_rubisco_carboxylase;
Pm.R_RBC_h_duplicate_3_enzyme.keff_f = keff_rubisco_carboxylase;
Pm.R_RBC_h_duplicate_4_enzyme.keff_f = keff_rubisco_carboxylase;
Pm.R_RBO_h_enzyme.keff_f = keff_rubisco_oxygenase;
Pm.R_RBO_h_duplicate_2_enzyme.keff_f= keff_rubisco_oxygenase;
Pm.R_RBO_h_duplicate_3_enzyme.keff_f= keff_rubisco_oxygenase;
Pm.R_RBO_h_duplicate_4_enzyme.keff_f= keff_rubisco_oxygenase;

Pm.R_PSII_h_enzyme.keff_f= keff_psiII;
Pm.R_PSII2_h_enzyme.keff_f= keff_psiII;
Pm.R_PSII3_h_enzyme.keff_f= keff_psiII;
Pm.R_PSII4_h_enzyme.keff_f= keff_psiII;
