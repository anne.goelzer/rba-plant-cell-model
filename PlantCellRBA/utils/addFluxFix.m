function Matrices = addFluxFix(idF,offset, TargetsMF,Reac,Matrices)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Research software: PlantCellRBA
% @ Copyright (c) INRAE
% Author: Anne Goelzer (INRAE), anne.goelzer@inrae.fr
% LICENCE: EUPL v1.2 (https://eupl.eu/1.2/en/)
% Requires: MATLAB and IBM ILOG CPLEX (v12.6 or v12.8) licences for code running
% Creation and last modification: 12 May 2023
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Aeq0 = Matrices.Aeq0;
Aeq1 = Matrices.Aeq1;
A0 = Matrices.A0;
A1 = Matrices.A1;
beq0 = Matrices.beq0;
beq1 = Matrices.beq1;
b0 = Matrices.b0;
b1 = Matrices.b1;
name_Aeq = Matrices.name_Aeq;
name_A = Matrices.name_A;

Nv = size(Aeq1,2);
tmpAl = zeros(1,Nv);
listFluxFix = fieldnames(TargetsMF.(idF).reaction_fluxes);
if isfield(Reac,listFluxFix{1})
    if isfield(TargetsMF.(idF).reaction_fluxes.(listFluxFix{1}),'value')
        % if equality 
        tmpAl(offset+Reac.(listFluxFix{1}).number) = 1;
        Aeq0 = [Aeq0; tmpAl];
        Aeq1 = [Aeq1; zeros(1,Nv)];
        beq0 = [beq0; 0];
        beq1 = [beq1; 0];
        name_Aeq = cat(2,name_Aeq,{idF});
        
    elseif  isfield(TargetsMF.(idF).reaction_fluxes.(listFluxFix{1}),'lowerBound')
        % if inequality
        tmpAl(offset+Reac.(listFluxFix{1}).number) = -1;
        A0 = [A0; tmpAl];
        A1 = [A1; zeros(1,Nv)];
        b0 = [b0; 0];
        b1 = [b1; 0];
        name_A = cat(2,name_A,{idF});
        
    elseif  isfield(TargetsMF.(idF).reaction_fluxes.(listFluxFix{1}),'upperBound')
        % if inequality
        tmpAl(offset+Reac.(listFluxFix{1}).number) = 1;
        A0 = [A0; tmpAl];
        A1 = [A1; zeros(1,Nv)];
        b0 = [b0; 0];
        b1 = [b1; 0];
        name_A = cat(2,name_A,{idF});
    end
else
    disp('Unknown reaction')
end

Matrices.Aeq0 = Aeq0;
Matrices.Aeq1 = Aeq1;
Matrices.A0 = A0;
Matrices.A1 = A1;
Matrices.beq0 = beq0;
Matrices.beq1 = beq1;
Matrices.b0 = b0;
Matrices.b1 = b1;
Matrices.name_Aeq = name_Aeq;
Matrices.name_A = name_A;


