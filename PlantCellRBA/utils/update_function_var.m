function ParameterFunctions_m = update_function_var(ParameterFunctions,id_val, val_var)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Research software: PlantCellRBA
% @ Copyright (c) INRAE
% Author: Anne Goelzer (INRAE), anne.goelzer@inrae.fr
% LICENCE: EUPL v1.2 (https://eupl.eu/1.2/en/)
% Requires: MATLAB and IBM ILOG CPLEX (v12.6 or v12.8) licences for code running
% Creation and last modification: 12 May 2023
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
listeF = fieldnames(ParameterFunctions);
for i=1:length(listeF)
    idM = ParameterFunctions.(listeF{i}).var;
    if strcmp(idM,id_val)
        ParameterFunctions.(listeF{i}).var_value = val_var;
    end
end

ParameterFunctions_m = ParameterFunctions;
