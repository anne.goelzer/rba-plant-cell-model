function [Comp,Pg,AAtot] = update_density_Pg(Comp,Prot,Parameters,ParameterFunctions)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Research software: PlantCellRBA
% @ Copyright (c) INRAE
% Author: Anne Goelzer (INRAE), anne.goelzer@inrae.fr
% LICENCE: EUPL v1.2 (https://eupl.eu/1.2/en/)
% Requires: MATLAB and IBM ILOG CPLEX (v12.6 or v12.8) licences for code running
% Creation and last modification: 12 May 2023
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
listeC = fieldnames(Comp);
Nc = length(listeC);

% Compute AAtot
AAtot = eval_function(ParameterFunctions.amino_acid_concentration);

Pg_tot = 0;
Pg_aa_tot = 0;
listeC2 = listeC;

% Compute max density
for i=1:length(listeC2)
   idC_density = [listeC2{i} '_density'];
   try
        Comp.(listeC2{i}).density_aa = compute_param(Parameters.(idC_density),ParameterFunctions);
   catch
       try
           Comp.(listeC2{i}).density_aa = eval_function(ParameterFunctions.(idC_density));
       catch 
           
       end
       %disp('Field absent in Parameters:')
       %idC_density
   end
   
   % compute Pg
   idC_Pg = ['nonenzymatic_proteins_' listeC2{i}];
   idP_Pg = ['average_protein_' listeC2{i}];
   try
        Comp.(listeC2{i}).Pg = compute_param(Parameters.(idC_Pg),ParameterFunctions);
        Pg_tot = Pg_tot + Comp.(listeC2{i}).Pg;
   catch
       disp('Field absent in Parameters:')
       idC_Pg
   end
   
   % compute Pg in AA residues 
   try
       Comp.(listeC2{i}).aa_Pg = Prot.(idP_Pg).aa_tot*Comp.(listeC2{i}).Pg;
       Pg_aa_tot = Pg_aa_tot + Prot.(idP_Pg).aa_tot*Comp.(listeC2{i}).Pg;
   catch
       disp('Field absent in Proteins:')
       idP_Pg
   end
end


Pg.c_tot = Pg_tot;
Pg.aa_tot = Pg_aa_tot;

        
