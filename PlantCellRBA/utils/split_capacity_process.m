function capP = split_capacity_process(P,Process)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Research software: PlantCellRBA
% @ Copyright (c) INRAE
% Author: Anne Goelzer (INRAE), anne.goelzer@inrae.fr
% LICENCE: EUPL v1.2 (https://eupl.eu/1.2/en/)
% Requires: MATLAB and IBM ILOG CPLEX (v12.6 or v12.8) licences for code running
% Creation and last modification: 12 May 2023
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% list of map component 
MapPM = fieldnames(Process.processing_map.PM);
Nmp = length(MapPM);
capP = 0;

% compute capacity cost 
for i=1:Nmp
    if isfield(P.composition,MapPM{i})
        capP = capP + P.composition.(MapPM{i})*Process.processing_map.PM.(MapPM{i}).scaling;       
    end   
end

