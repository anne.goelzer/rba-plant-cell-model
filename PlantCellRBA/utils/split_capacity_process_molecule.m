function capP = split_capacity_process_molecule(mol,Process)      
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Research software: PlantCellRBA
% @ Copyright (c) INRAE
% Author: Anne Goelzer (INRAE), anne.goelzer@inrae.fr
% LICENCE: EUPL v1.2 (https://eupl.eu/1.2/en/)
% Requires: MATLAB and IBM ILOG CPLEX (v12.6 or v12.8) licences for code running
% Creation and last modification: 12 May 2023
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


capP = 0;
if ~isempty(mol)
     % determine components to be produced
    [test,ind] = is_input_of(mol.id,Process);

    if test
        % determine components 
        try
            switch  Process.processing_map.set
                case 'protein'
                    capP = capP + split_capacity_process(mol, Process);
                case 'rna'
                    capP = capP + split_capacity_process(mol,Process);
                case 'dna'
                    capP = capP + split_capacity_process(mol,Process);
                    
                otherwise
                    disp('the set of molecules to be produced is unknown')
            end
        catch
            keyboard
        end
    end
    
end
    

