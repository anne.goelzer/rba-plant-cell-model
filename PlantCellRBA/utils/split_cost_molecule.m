function vecCostM = split_cost_molecule(idM,M,ProcessFull, Metab,type)      
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Research software: PlantCellRBA
% @ Copyright (c) INRAE
% Author: Anne Goelzer (INRAE), anne.goelzer@inrae.fr
% LICENCE: EUPL v1.2 (https://eupl.eu/1.2/en/)
% Requires: MATLAB and IBM ILOG CPLEX (v12.6 or v12.8) licences for code running
% Creation and last modification: 12 May 2023
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Metab
listeMetab = fieldnames(Metab);
Nx = length(listeMetab);

% process
listePFull = fieldnames(ProcessFull);


% compute cost of a molecule
vecCostM = zeros(Nx,1);
if ~isempty(idM)
    % determine components to be produced
    [ind] = is_input_of_Pfull(idM,ProcessFull);
    for k=1:size(ind,1)
        ProP = ProcessFull.(listePFull{ind(k,1)});
        % Degradation or production? 
        switch  ProP.type_process
            case 'listOfProductions'
                switch type
                    case 'P'
                        f_mul = 1;
                    case 'D'
                        f_mul = 0;
                    otherwise
                        error('unknown type')
                end

            case 'listOfDegradations'
                switch type
                    case 'P'
                        f_mul = 0;
                    case 'D'
                        f_mul = -1;
                    otherwise
                        error('unknown type')
                end
            
            otherwise
                disp(' unknown type')
        end
        
        % determine cost of production
        vecCost = split_cost(M, ProP,Metab);
        
        % on calcule le vecteur de cout pour l'element M
        vecCostM = vecCostM + vecCost*f_mul;

        clear vecCost
        
    end
    
 
end
