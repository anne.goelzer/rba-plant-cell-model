function resultat = eval_function(Functions)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Research software: PlantCellRBA
% @ Copyright (c) INRAE
% Author: Anne Goelzer (INRAE), anne.goelzer@inrae.fr
% LICENCE: EUPL v1.2 (https://eupl.eu/1.2/en/)
% Requires: MATLAB and IBM ILOG CPLEX (v12.6 or v12.8) licences for code running
% Creation and last modification: 12 May 2023
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
switch Functions.typeF
    case 'constant'
        resultat  = eval_constant(Functions.listOfParameters);
        
    case 'linear'
        resultat = eval_linear(Functions.listOfParameters,Functions.var_value);
        
    case 'michaelisMenten'
        resultat = eval_michaelisMenten(Functions.listOfParameters,Functions.var_value);
        
    case 'exponential'
        resultat = eval_exponential(Functions.listOfParameters,Functions.var_value);
    otherwise
        error('functions a creer')
end



function a = eval_michaelisMenten(Func,var)
b = Func.kmax*(var/(var+Func.Km));
if isfield(Func,'Y_MIN')
    a = max(b,Func.Y_MIN);
else
    a=b;   
end

function a = eval_exponential(Func,var)
a = exp(Func.RATE*var);

function a = eval_constant(Func)
a = Func.CONSTANT;

function a = eval_linear(Func,var)
var_r = max(Func.X_MIN,min(var,Func.X_MAX));
a_r = Func.LINEAR_CONSTANT + Func.LINEAR_COEF*var_r;
a = max(Func.Y_MIN,min(a_r,Func.Y_MAX));
