function Medium = import_medium(idEx)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Research software: PlantCellRBA
% @ Copyright (c) INRAE
% Author: Anne Goelzer (INRAE), anne.goelzer@inrae.fr
% LICENCE: EUPL v1.2 (https://eupl.eu/1.2/en/)
% Requires: MATLAB and IBM ILOG CPLEX (v12.6 or v12.8) licences for code running
% Creation and last modification: 12 May 2023
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fp = fopen('medium.tsv');
if fp == -1
    disp('pb opening file medium')
else
    tmpL = fgetl(fp);
    C = textscan(fp,'%s\t%f\n');
    fclose(fp);
    
    % on ajoute le suffixe du comaprtiment exterieur qui va bien
    n = length(C{1});
    toto = cell(n,1);
    
    for i=1:n,   toto{i} = ['_' idEx];    end
        C1_mod = cellfun(@strcat,C{1},toto,'UniformOutput',false);
        Medium = cell2struct(num2cell(C{2}), C1_mod,1);
     
   
end
