function [vec_ind] = is_input_of_Pfull(P,Process)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Research software: PlantCellRBA
% @ Copyright (c) INRAE
% Author: Anne Goelzer (INRAE), anne.goelzer@inrae.fr
% LICENCE: EUPL v1.2 (https://eupl.eu/1.2/en/)
% Requires: MATLAB and IBM ILOG CPLEX (v12.6 or v12.8) licences for code running
% Creation and last modification: 12 May 2023
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% fonction testing if a component is input of a Process 
ListeP = fieldnames(Process);
Np = length(ListeP);
vec_ind = [];
for i=1:Np
    [test,ind] = is_input_of(P,Process.(ListeP{i}));

    if test 
        vec_ind = [vec_ind; i ind];
    end
end

