function Pm = update_max_efficiency(Pm,Parameters,ParameterFunctions)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Research software: PlantCellRBA
% @ Copyright (c) INRAE
% Author: Anne Goelzer (INRAE), anne.goelzer@inrae.fr
% LICENCE: EUPL v1.2 (https://eupl.eu/1.2/en/)
% Requires: MATLAB and IBM ILOG CPLEX (v12.6 or v12.8) licences for code running
% Creation and last modification: 12 May 2023
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
listeF = fieldnames(Pm);
Nm = length(listeF);
for i=1:Nm
    
    % forward
    idF = Pm.(listeF{i}).keff_forward;
    if isfield(Parameters,idF)
       Pm.(listeF{i}).keff_f = compute_param(Parameters.(idF),ParameterFunctions);
    else
        Pm.(listeF{i}).keff_f =  eval_function(ParameterFunctions.(idF));
    end
    
    % reverse
    idF = Pm.(listeF{i}).keff_backward;
    if isfield(Parameters,idF)
       Pm.(listeF{i}).keff_b = compute_param(Parameters.(idF),ParameterFunctions);
    else
        Pm.(listeF{i}).keff_b =  eval_function(ParameterFunctions.(idF));
    end
 
end
