function vecCost = split_cost(P,Process, Metab)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Research software: PlantCellRBA
% @ Copyright (c) INRAE
% Author: Anne Goelzer (INRAE), anne.goelzer@inrae.fr
% LICENCE: EUPL v1.2 (https://eupl.eu/1.2/en/)
% Requires: MATLAB and IBM ILOG CPLEX (v12.6 or v12.8) licences for code running
% Creation and last modification: 12 May 2023
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

listeM = fieldnames(Metab);
Nx = length(listeM);

% init vec
vecCost = zeros(Nx,1);

% list of P component
CompoP = fieldnames(P.composition);
Ncp = length(CompoP);

% list of map component 
MapPM = fieldnames(Process.processing_map.PM);
Nmp = length(MapPM);

% compute cost
for i=1:Nmp
    if isfield(P.composition,MapPM{i})
         % if precursors, import the number of component 
        nC = P.composition.(MapPM{i});       
    elseif strcmp(MapPM{i},'constantProcessing')
        % if constant processing,  nC=1
        nC = 1;
    else
        disp('compose non reconnu')   
        nC = 0;
    end
   % Look if metabolites are produced/consumed in the map 
    if isfield(Process.processing_map.PM.(MapPM{i}),'idMetab')
        Nmet = length(Process.processing_map.PM.(MapPM{i}).idMetab);
        for j=1:Nmet
            % compute cost: nC*individual cost per metab  
            % by defaut: Process.processing_map.PM has a negative stoichiometry
            % for reactants and positive for products
            idM = Process.processing_map.PM.(MapPM{i}).idMetab{j};
            vecCost(Metab.(idM).number_S,1) = vecCost(Metab.(idM).number_S,1) + nC*Process.processing_map.PM.(MapPM{i}).stoichiometry(j);
        end
    end
    
end

