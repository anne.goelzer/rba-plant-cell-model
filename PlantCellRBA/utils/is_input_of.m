function [test,ind] = is_input_of(P,Process)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Research software: PlantCellRBA
% @ Copyright (c) INRAE
% Author: Anne Goelzer (INRAE), anne.goelzer@inrae.fr
% LICENCE: EUPL v1.2 (https://eupl.eu/1.2/en/)
% Requires: MATLAB and IBM ILOG CPLEX (v12.6 or v12.8) licences for code running
% Creation and last modification: 12 May 2023
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if isfield(Process,'listOfTargets')
    ListeInput = fieldnames(Process.listOfTargets);
    Nt = length(ListeInput);
    ind = max(strcmp(P,ListeInput).*[1:Nt]');
    if ind> 0
        test = true;
    else
        test = false;
    end
    
else
    test = false;
    ind = 0;
end



