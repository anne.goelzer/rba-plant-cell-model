function capP = split_capacity_process_machine(MM,Process)      
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Research software: PlantCellRBA
% @ Copyright (c) INRAE
% Author: Anne Goelzer (INRAE), anne.goelzer@inrae.fr
% LICENCE: EUPL v1.2 (https://eupl.eu/1.2/en/)
% Requires: MATLAB and IBM ILOG CPLEX (v12.6 or v12.8) licences for code running
% Creation and last modification: 12 May 2023
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
capP = 0;
if ~isempty(MM)
     % determine components to be produced
    Ngg = length(MM.idC);
    for j=1:Ngg
        [test,ind] = is_input_of(MM.idC{j},Process);
        if test
           % determine components 
            try
                switch  Process.processing_map.set
                    case 'protein'
                        capP = capP + split_capacity_process(MM.Prot.(MM.idC{j}), Process)*MM.stoichiometry(j);
                    case 'rna'
                        capP = capP + split_capacity_process(MM.RNA.(MM.idC{j}),Process)*MM.stoichiometry(j);
                    case 'dna'
                        capP = capP + split_capacity_process(MM.DNA.(MM.idC{j}),Process)*MM.stoichiometry(j);
                        
                    otherwise
                        disp('the set of molecules to be produced is unknown')
                end
            catch
                keyboard
            end
        end
        
    end
end
    

