function vecCostMM = split_cost_machine(MM,ProcessFull, Metab,type)      
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Research software: PlantCellRBA
% @ Copyright (c) INRAE
% Author: Anne Goelzer (INRAE), anne.goelzer@inrae.fr
% LICENCE: EUPL v1.2 (https://eupl.eu/1.2/en/)
% Requires: MATLAB and IBM ILOG CPLEX (v12.6 or v12.8) licences for code running
% Creation and last modification: 12 May 2023
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% type can be 'P' ou 'D' and will set if we compute cost of production or degradation 

% Metab
listeMetab = fieldnames(Metab);
Nx = length(listeMetab);

% pocess
listePFull = fieldnames(ProcessFull);


vecCostMM = zeros(Nx,1);
if ~isempty(MM)
    % number of components to be produced 
    Ngg = length(MM.idC);
    for j=1:Ngg
        [ind] = is_input_of_Pfull(MM.idC{j},ProcessFull);
        for k=1:size(ind,1)
            ProP = ProcessFull.(listePFull{ind(k,1)});
            % Degradation or production? 
            switch  ProP.type_process
                case 'listOfProductions'
                    switch type
                        case 'P'
                            f_mul = 1;
                        case 'D'
                            f_mul = 0;
                        otherwise
                            error('type non reconnu')
                    end
 
                case 'listOfDegradations'
                    switch type
                        case 'P'
                            f_mul = 0;
                        case 'D'
                            f_mul = -1;
                        otherwise
                            error('type non reconnu')
                    end
 
                otherwise
                    disp(' cas type process non reconnu')
            end
            
            % compute cost
            vecCost= zeros(Nx,1);
            % determine components 
            try
                switch  ProP.processing_map.set
                    case 'protein'
                        % on recupere les proteins
                        vecCost = split_cost(MM.Prot.(MM.idC{j}), ProP,Metab);
                    case 'rna'
                        % on recupere les RNA
                        vecCost = split_cost(MM.RNA.(MM.idC{j}), ProP,Metab);
                    case 'dna'
                        % on recupere les DNA
                        vecCost = split_cost(MM.DNA.(MM.idC{j}), ProP,Metab);
                        
                    otherwise
                        disp(' cas set process non reconnu')
                end
            catch
                keyboard
            end
            
            % Add cost of component 
            vecCostMM = vecCostMM + vecCost*f_mul*MM.stoichiometry(j);
            
            % on clear vecCost
            clear vecCost
        end
    end
    
    % Determine if metabolites are consumed/produced for machine assembly 
    % if yes, the field  "Met" is present 
    if isfield(MM,'Met')
        pp = fieldnames(MM.Met);
        Nmet = length(pp);
        vecCost = zeros(Nx,1);
        for j=1:Nmet
            idM = pp{j};
            vecCost(Metab.(idM).number_S,1) = vecCost(Metab.(idM).number_S,1) + MM.Met.(idM).stoichiometry;
        end
        
        vecCostMM = vecCostMM + vecCost;
    end
    
    
end
