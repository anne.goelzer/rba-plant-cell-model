function resultat = eval_param(Param,Functions,val)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Research software: PlantCellRBA
% @ Copyright (c) INRAE
% Author: Anne Goelzer (INRAE), anne.goelzer@inrae.fr
% LICENCE: EUPL v1.2 (https://eupl.eu/1.2/en/)
% Requires: MATLAB and IBM ILOG CPLEX (v12.6 or v12.8) licences for code running
% Creation and last modification: 12 May 2023
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

nF = length(Param.listOfFunctions);
vec_result = zeros(1,nF);

for i=1:nF
    idF = Param.listOfFunctions{i}.id;
    switch Functions.(idF).typeF
        case 'constant'
            vec_result(i) = eval_constant(Functions.(idF).listOfParameters);
            
        case 'linear'
            vec_result(i) = eval_linear(Functions.(idF).listOfParameters,val);
            
        case 'michaelisMenten'
            vec_result(i) = eval_michaelisMenten(Functions.(idF).listOfParameters,val);
            
        case 'exponential'
            vec_result(i) = eval_exponential(Functions.(idF).listOfParameters,val);
        otherwise
            error('functions a creer')
    end
end

% test which arithmetic operation has to be performed
switch Param.typeF
    case 'multiplication'
        resultat = prod(vec_result);
        
    case 'addition'
        resultat = sum(vec_result);
        
    otherwise
        error('Type d''aggregaton non gerees')
    
end

function a = eval_michaelisMenten(Func,var)
a = Func.kmax*(var/(var+Func.Km));

function a = eval_exponential(Func,var)
a = exp(Func.RATE*var);

function a = eval_constant(Func)
a = Func.CONSTANT;

function a = eval_linear(Func,var)
var_r = max(Func.X_MIN,min(var,Func.X_MAX));
a_r = Func.LINEAR_CONSTANT + Func.LINEAR_COEF*var_r;
a = max(Func.Y_MIN,min(a_r,Func.Y_MAX));
