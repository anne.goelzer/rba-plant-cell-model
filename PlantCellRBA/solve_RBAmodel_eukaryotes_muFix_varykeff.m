function [RBAopt] = solve_RBAmodel_eukaryotes_muFix_varykeff(RBAmodel,Init_RBA,optionsRBA,mu_test,keff_rand)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Research software: PlantCellRBA
% @ Copyright (c) INRAE
% Author: Anne Goelzer (INRAE), anne.goelzer@inrae.fr
% LICENCE: EUPL v1.2 (https://eupl.eu/1.2/en/)
% Requires: MATLAB and IBM ILOG CPLEX (v12.6 or v12.8) licences for code running
% Creation and last modification: 12 May 2023
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

kCrand = keff_rand.kC_random;
kTrand = keff_rand.kT_random;
kR1rand = keff_rand.kRpol1_random;
kR2rand = keff_rand.kRpol2_random;
kR3rand = keff_rand.kRpol3_random;
kDrand = keff_rand.kDpol_random;

%-------------
% struc RBA
%------------
Comp = Init_RBA.Comp;
Metab = Init_RBA.Metab;
Parameters = Init_RBA.Parameters;
ParameterFunctions = Init_RBA.ParameterFunctions_m;
TargetsMF = Init_RBA.TargetsMF;
Prot = Init_RBA.Prot;
listeComp = fieldnames(Comp);
status_nu = Init_RBA.status_nu;

% restore matrixes and structures
Matrices = RBAmodel.matrix;
Process = RBAmodel.Process;
Pm = RBAmodel.Pm; 
listeProcess = fieldnames(Process);
Sind = RBAmodel.Sind; 


% restore indexes
Nm = Sind.Nm;
Nc= Sind.Nc;
offset_flux = Sind.offset_flux;
offset_concentration = Sind.offset_concentration;
offset_volume = Sind.offset_volume;
ind_iq_D = Sind.ind_iq_D;
ind_iq_Pro = Sind.ind_iq_Pro;
ind_iq_nu = Sind.ind_iq_nu;
ind_eq_X =Sind.ind_eq_X;
ind_eq_Y = Sind.ind_eq_Y;
Npro = Sind.Npro;
Nx = Sind.Nx;
Ntg = Sind.Ntg;
Ny = Sind.Ny;
ind_uT = Sind.ind_uT;
ind_eq_Tfix = Sind.ind_eq_Tfix;
ind_duplicate = Sind.ind_duplicate;
Nlig_add = Sind.Nlig_add;

% options
options = cplexoptimset('cplex');
options.feasopt.tolerance = 1e-9;
options.simplex.tolerances.feasibility = 1e-9;
options.simplex.tolerances.optimality = 1e-9;
options.simplex.tolerances.markowitz = 0.1;
options.barrier.convergetol = 1e-9;
options.read.scale = -1;
%options.lpmethod = 4;
options.display = optionsRBA.display_cplex;
%-----------------------------------------
% init optimisation
%-----------------------------------------
% Bound on variables



Aeq0 = Matrices.Aeq0;
Aeq1 = Matrices.Aeq1;
A0 = Matrices.A0;
A1 = Matrices.A1;
beq0 = Matrices.beq0;
beq1 = Matrices.beq1;
b0 = Matrices.b0;
b1 = Matrices.b1;
name_Aeq = Matrices.name_Aeq;
name_A = Matrices.name_A;
name_Xv = Matrices.name_Xv;
LB = Matrices.LB;
UB = Matrices.UB;
f = Matrices.f;
Aeq0_mean_complex = Matrices.Aeq0_mean_complex;
A0_mean_complex = Matrices.A0_mean_complex;

nA = size(A0,1);
nAeq = size(Aeq0,1);


nA = size(A0,1);
nAeq = size(Aeq0,1);

TargetProt = fieldnames(TargetsMF.translation_targets.concentration);
TargetRNA = fieldnames(TargetsMF.transcription_targets.concentration);
TargetDNA = fieldnames(TargetsMF.replication_targets.concentration);




% LP matrixes
A0_m = A0_mean_complex;
Aeq0_m = Aeq0_mean_complex;
A1_m = A1;
b0_m = b0;
b1_m = b1;
Aeq1_m = Aeq1;
beq0_m = beq0;
beq1_m = beq1;
name_A2 = name_A;
name_Aeq2 = name_Aeq;
name_Xv2 = name_Xv;

% Update ParameterFunctions with mu 
ParameterFunctions_mu = update_function_var(ParameterFunctions,'growth_rate', mu_test);

% Update Pg with mu 
[Comp,Pg_tot,AAtot] = update_density_Pg(Comp,Prot,Parameters,ParameterFunctions_mu);

Comp.Secreted.density_aa = Comp.Secreted.aa_Pg;

% Update Targets
for i=1:length(TargetProt)
    p_value =  TargetsMF.translation_targets.concentration.(TargetProt{i}).value;
    Ptot_value = compute_param(Parameters.(p_value),ParameterFunctions_mu);
    pp = max(strcmp(TargetProt{i},name_Aeq).*[1:nAeq]);
    beq0_m(pp) = Ptot_value;
end
for i=1:length(TargetRNA)
    p_value =  TargetsMF.transcription_targets.concentration.(TargetRNA{i}).value;
    Rtot_value = eval_function(ParameterFunctions_mu.(p_value));
    pp = max(strcmp(TargetRNA{i},name_Aeq).*[1:nAeq]);
    beq0_m(pp) = Rtot_value;
end
for i=1:length(TargetDNA)
    p_value =  TargetsMF.replication_targets.concentration.(TargetDNA{i}).value;
    Dtot_value = eval_function(ParameterFunctions_mu.(p_value));
    pp = max(strcmp(TargetDNA{i},name_Aeq).*[1:nAeq]);
    beq0_m(pp) = Dtot_value;
end



% Update efficiencies of macromolecular processes 
for i=1:Npro
        if strcmpi(Process.(listeProcess{i}).efficiency.keff,'chaperone_efficiency_CM')
            kM = kCrand(i);
        elseif strcmpi(Process.(listeProcess{i}).efficiency.keff,'ribosome_efficiency_CM')
            kM = kTrand;
        elseif strcmpi(Process.(listeProcess{i}).efficiency.keff,'RNApol1_efficiency_CM')
            kM = kR1rand;
        elseif strcmpi(Process.(listeProcess{i}).efficiency.keff,'RNApol2_efficiency_CM')
            kM = kR2rand;
        elseif strcmpi(Process.(listeProcess{i}).efficiency.keff,'RNApol3_efficiency_CM')
            kM = kR3rand;
        elseif strcmpi(Process.(listeProcess{i}).efficiency.keff,'DNApol_efficiency_CM')
            kM = kDrand;
        else
                try
                        % evaluate the process efficiency
                        kM = compute_param(Parameters.(Process.(listeProcess{i}).efficiency.keff),ParameterFunctions_mu);
                catch
                        kM = eval_function(ParameterFunctions_mu.(Process.(listeProcess{i}).efficiency.keff));
                end
        end
        % add -kM on each machine 
        A0_m(ind_iq_Pro+i,offset_concentration + Nm + i) =              A0_m(ind_iq_Pro+i,offset_concentration + Nm + i) -kM;
    
end


% Density constraints
for i=1:Nc
    id_C_density = listeComp{i};
    A0_m(ind_iq_D + i,offset_volume + i) = -AAtot;
    
end


% Update targets
listeMFix = fieldnames(TargetsMF.metabolite_production.concentration);
listeMCFix = fieldnames(TargetsMF.macrocomponent_production.concentration);
AllCFix = {listeMFix{:},  listeMCFix{:}};

for i=1:length(AllCFix)
    idM = AllCFix{i};
    if isfield(Metab,idM)
        if max(strcmp(idM,listeMFix).*[1:length(listeMFix)]')
            idV = TargetsMF.metabolite_production.concentration.(idM).value;
        elseif max(strcmp(idM,listeMCFix).*[1:length(listeMCFix)]')
            idV = TargetsMF.macrocomponent_production.concentration.(idM).value;
        elseif  max(strcmp(idM,listeTSCFix).*[1:length(listeTSCFix)]')
            idV = TargetsMF.transcription_targets.concentration.(idM).value;
        elseif max(strcmp(idM,listeREPFix).*[1:length(listeREPFix)]')
            idV = TargetsMF.replication_targets.concentration.(idM).value;
        end
        try
            beq1_m(ind_eq_X + Metab.(idM).number_S,1) =  beq1_m(ind_eq_X + Metab.(idM).number_S,1) +  eval_function(ParameterFunctions_mu.(idV));
        catch
            try
                beq1_m(ind_eq_X + Metab.(idM).number_S,1) =  beq1_m(ind_eq_X + Metab.(idM).number_S,1) +  compute_param(Parameters.(idV),ParameterFunctions_mu);
            catch
                disp('Pb in parameters')
            end
        end
    else
        disp('Metab non trouve ')
        idM
    end
    clear idV idM
end


    %----------------------------------------------------------------------------------
    % Add ATP maintenance fluxes
if isfield(TargetsMF.maintenance_atp_target.reaction_fluxes.R_maintenance_atp,'value')
    ll = max(strcmp('maintenance_atp_target',name_Aeq).*[1:nAeq]);
    idV = TargetsMF.maintenance_atp_target.reaction_fluxes.R_maintenance_atp.value;
    beq0_m(ll) = beq0_m(ll) + eval_function(ParameterFunctions_mu.(idV));
elseif isfield(TargetsMF.maintenance_atp_target.reaction_fluxes.R_maintenance_atp,'lowerBound')
    ll = max(strcmp('maintenance_atp_target',name_A).*[1:nA]);
    idV = TargetsMF.maintenance_atp_target.reaction_fluxes.R_maintenance_atp.lowerBound;
    b0_m(ll) = b0_m(ll) - eval_function(ParameterFunctions_mu.(idV));
elseif isfield(TargetsMF.maintenance_atp_target.reaction_fluxes.R_maintenance_atp,'upperBound')
    ll = max(strcmp('maintenance_atp_target',name_A).*[1:nA]);
    idV = TargetsMF.maintenance_atp_target.reaction_fluxes.R_maintenance_atp.upperBound;
    b0_m(ll) = b0_m(ll) + eval_function(ParameterFunctions_mu.(idV));
end



% Update matrixes with mu
A = A0_m + A1_m*mu_test;
b = b0_m + b1_m*mu_test;
Aeq = Aeq0_m + Aeq1_m*mu_test;
beq = beq0_m + beq1_m*mu_test;

DataOpt.ind_eq_X = ind_eq_X;
DataOpt.Nx = Nx;
DataOpt.Ny = Ny;
DataOpt.Ntg = Ntg;
DataOpt.Npro = Npro;
DataOpt.Nc= Nc;
DataOpt.Nm= Nm;
DataOpt.offset_concentration = offset_concentration;
DataOpt.offset_flux = offset_flux;
DataOpt.ind_uT = ind_uT;
DataOpt.ind_iq_Pro = ind_iq_Pro;
DataOpt.ind_eq_Y = ind_eq_Y;
DataOpt.ind_eq_Tfix = ind_eq_Tfix;
DataOpt.ind_iq_D = ind_iq_D;
DataOpt.ind_duplicate = ind_duplicate;
DataOpt.Nlig_add = Nlig_add;
DataOpt.Aeq = Aeq;
DataOpt.beq = beq;
DataOpt.A = A;
DataOpt.b = b;
DataOpt.LB = LB;
DataOpt.UB = UB;
DataOpt.f = f;
DataOpt.Pm = Pm;
DataOpt.status_nu = status_nu;
DataOpt.name_Xv = name_Xv;
DataOpt.name_Aeq = name_Aeq;
DataOpt.name_A = name_A;

% elimine u
DataOptRed = elimine_u(DataOpt);

% Test 1
[X,fval,exitflag,output,lambda]=cplexlp(DataOptRed.f,DataOptRed.A,DataOptRed.b,DataOptRed.Aeq,DataOptRed.beq,DataOptRed.LB,DataOptRed.UB,[],options);

if strcmpi(optionsRBA.display_iter_mu,'on')
    disp(['mu iter:' num2str(mu_test) ', exitflag:' num2str(exitflag2) ', objective:' num2str(fval2)])
end

if exitflag2==1 || exitflag2==5
    % feasability ok
    mu_min = mu_test; 
    RBAopt.X2 = X;
    RBAopt.A2 = DataOptRed.A;
    RBAopt.b2 = DataOptRed.b;
    RBAopt.Aeq2 = DataOptRed.Aeq;
    RBAopt.beq2 = DataOptRed.beq;
    RBAopt.LB2 = DataOptRed.LB;
    RBAopt.UB2 = DataOptRed.UB;
    RBAopt.f2 = DataOptRed.f;
    RBAopt.Pm = Pm;
    RBAopt.Pg_tot = Pg_tot;
    RBAopt.AAtot = AAtot;
    RBAopt.Process = Process;
    RBAopt.Comp = Comp;
    RBAopt.lambda = lambda2;
    RBAopt.mu_opt = mu_min;
    RBAopt.ParameterFunctions_mu = ParameterFunctions_mu;
    RBAopt.Sind = Sind;
    RBAopt.name_A = name_A;
    RBAopt.name_Aeq = name_Aeq;
    RBAopt.name_Xv = name_Xv;
    RBAopt.name_A2 = DataOptRed.name_A;
    RBAopt.name_Aeq2 = DataOptRed.name_Aeq;
    RBAopt.name_Xv2 = DataOptRed.name_Xv;
    RBAopt.status_enz_irrev = DataOptRed.status_enz_irrev;
    

    disp([' The value mu=( ' num2str(mu_test) ' ) is feasible'])
    
else
   disp(['The value mu=( ' num2str(mu_test) ' ) is infeasible'])
    RBAopt = [];
end





%toc


