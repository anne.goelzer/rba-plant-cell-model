# RBA plant cell model

## Description
This project contains 
- RBA-xml files: the RBA leaf cell model of A. thaliana that is encoded in XML files (compatible with the software RBApy).
For further information on RBA or RBApy, please visit https://rba.inrae.fr/.
Be careful: at this stage, RBApy is not yet able to support simulation for eukaryotic cells (Goelzer et al. BioRXiv 2019; 750182).Use the PlantCellRBA software instead for simulating the RBA model.
- PlantCellRBA: the PlantCellRBA software for simulating the RBA leaf model (Requires Matlab 2018 and Cplex 12.8).
- Farquhar_arabette: the Farqhuar model and simulations for A. thaliana

## Installation
Clone the repository. 
Install Cplex 12.8 (or Cplex 12.6) if you want to run the model in Matlab. 
Make sure that Cplex can be accessed by Matlab

## Authors and acknowledgment
The RBA leaf cell model, the PlantCellRBA software and the Farquhar model of Arabidopsis thaliana are developed by Anne Goelzer (INRAE). 

## License
This project is under the licence CC-BY-NC.
PlantCellRBA is distributed under the EUPL v1.2 licence (see the file licence.txt in the PlantCellRBA directory)


