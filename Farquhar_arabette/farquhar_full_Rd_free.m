function [A,Ac,Aj,Ap] = farquhar_full_Rd_free(T,Rt,Rd,Tp,I,O,C)

T_kelvin = T + 273.15;
R = 8.31; % J/K/mol



% Parameters
init_parameters_consolides


gamma_s = 0.5*O/Sco;% en bar
% Rubisco limited assimilation rate
% Ac
Ac = max(0,(C-gamma_s)*Vc/(C + Kc*(1+O/Ko)) - Rd);


% Aj
I2 = I*light_absorbance*(1-f)/2;
J = (I2 + Jmax - sqrt((I2 + Jmax)^2 - 4*theta*I2*Jmax))/2/theta;
Aj = max(0,(C-gamma_s)*J/(4*C + 8*gamma_s)-Rd);
% Ap: export limited CO2 assimilation rate
Ap = max(0,3*Tp - Rd);

AA = [Ac Aj Ap];
if ~isempty(AA)
    A = min(AA);
else
    A=0;
end


phi = ratio_Vomax_Vc_max*O*Kc/Ko/C;
disp(['phi: ' num2str(phi)])