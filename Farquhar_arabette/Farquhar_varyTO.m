close all
clear all

indO2 = 5; % O = 210mbar

% load result RBA
load Farquhar_RBA/RBA_varyTO.mat
nR = length(SolRBA);
nXX = length(SolRBA{indO2}.sol.name_Xv2);
ind_Y_rubisco = max(strcmp('Y_R_RBC_h_enzyme',SolRBA{indO2}.sol.name_Xv2).*[1:nXX]);
ind_Aass_CO2 = max(strcmp('nu_R_Im_CO2',SolRBA{indO2}.sol.name_Xv2).*[1:nXX]);
ind_Rd_mito = max(strcmp('nu_R_Tr_CO2m',SolRBA{indO2}.sol.name_Xv2).*[1:nXX]); % flux de CO2 excretes par la respiration
ind_Tp_chloro1 = max(strcmp('nu_R_Tr_X5P',SolRBA{indO2}.sol.name_Xv2).*[1:nXX]); % flux de P imported in chloroplast
ind_Tp_chloro2 = max(strcmp('nu_R_Tr_GPT1',SolRBA{indO2}.sol.name_Xv2).*[1:nXX]); % flux de P imported in chloroplast
ind_Tp_chloro3 = max(strcmp('nu_R_Tr_PPT',SolRBA{indO2}.sol.name_Xv2).*[1:nXX]); % flux de P imported in chloroplast
ind_Tp_chloro4 = max(strcmp('nu_R_Tr_TPT3',SolRBA{indO2}.sol.name_Xv2).*[1:nXX]); % flux de P imported in chloroplast
ind_Tp_chloro5 = max(strcmp('nu_R_Tr_TPT2',SolRBA{indO2}.sol.name_Xv2).*[1:nXX]); % flux de P imported in chloroplast
ind_Tp_chloro6 = max(strcmp('nu_R_Tr_TPT1',SolRBA{indO2}.sol.name_Xv2).*[1:nXX]); % flux de P imported in chloroplast
ind_Tp_chloro7 = max(strcmp('nu_R_Tr_NTT',SolRBA{indO2}.sol.name_Xv2).*[1:nXX]); % flux de P imported in chloroplast
ind_Tp_chloro8 = max(strcmp('nu_R_ATPase_h',SolRBA{indO2}.sol.name_Xv2).*[1:nXX]); % flux de P imported in chloroplast


% PhysioPlante.SLA -> specific leaf area in m2/gDW

conv_mM_day_in_uM_s = 1000/24/3600; 
conv_mM_day_in_mol_s = 1/1000/24/3600;

vecM = 'ox+*sdp';
 A_rba = [];
for i=1:nR

    figure(1)
    subplot(1,2,2)
    plot(gridT,SolRBA{i}.X(ind_Aass_CO2,:)/PhysioPlante.SLA*conv_mM_day_in_uM_s,['b' vecM(i) '-'])
    hold on
    title('RBA model','FontSize',24)
    ylabel('CO2 assimilation rate (umol/m2/s)','FontSize',24)
    xlabel('T(°C)','FontSize',24)
    set(gca,'XLim',[5 60],'Fontsize',24)
    grid on
       
    
    A_rba = [A_rba; SolRBA{i}.X(ind_Aass_CO2,:)/PhysioPlante.SLA*conv_mM_day_in_uM_s];
end




% % Farquhar simulation 
MW_rbcL = 52955; % Da (=g/mol)
MW_rbcS = 20286; % Da (=g/mol)
MW_rubisco = MW_rbcL*8 + MW_rbcS*8;% car composés de 8 sous-unités chacune


%Fix C and I
C =200e-6; % =200e-6 partial pressure of CO2, in bar
I = 1000e-6;% irradiance mol of photos/m2/s
vecT = [0 5 10 15 20 21 25 30 35 40 45 50 55 60]; nT = length(vecT);
vecO = [10 50 100 150 210 250 330]*1e-3;nO = length(vecO);
Ac = zeros(nO,nT);
Aj = zeros(nO,nT);
Ap = zeros(nO,nT);
A = zeros(nO,nT);
Rt = zeros(nO,nT);
Rd= zeros(nO,nT);
Tp= zeros(nO,nT);
legend_graph = {}
for j = 1:nO
    legend_graph = {legend_graph{:} [ num2str(vecO(j)/1e-3) ' mbar']};
    for i=1:nT
        T = vecT(i);
        O = vecO(j);

        % Rubisco content from RBA 
        Y_rubisco = SolRBA{j}.X(ind_Y_rubisco,i); % en mM/gDW
        % conversion en g/m2 = mM/gDW * g/mol * gDW/m2 
        Rt(j,i) = Y_rubisco*MW_rubisco/PhysioPlante.SLA/1000; 
        
        % Rd
        nu_Rd = abs(SolRBA{j}.X(ind_Rd_mito,i)); % mM/gDW/day
        % conversion en mol/m2/s
        Rd(j,i) = nu_Rd*conv_mM_day_in_mol_s/PhysioPlante.SLA;
        
                         % Tp
        nu_Tp = abs(SolRBA{j}.X(ind_Tp_chloro1,i) ...
            - SolRBA{j}.X(ind_Tp_chloro2,i) ...
            - SolRBA{j}.X(ind_Tp_chloro3,i) ...
            + SolRBA{j}.X(ind_Tp_chloro4,i) ...
            + SolRBA{j}.X(ind_Tp_chloro5,i) ...
            + SolRBA{j}.X(ind_Tp_chloro6,i) ...
            - SolRBA{j}.X(ind_Tp_chloro7,i)); % mM/gDW/day
        % conversion en mol/m2/s
        Tp(j,i) = nu_Tp*conv_mM_day_in_mol_s/PhysioPlante.SLA; 
        
        [A(j,i),Ac(j,i),Aj(j,i),Ap(j,i)] = farquhar_full_Rd_free(T,Rt(j,i),Rd(j,i),Tp(j,i),I,O,C);
        
        
    end
    
    figure(1)
    subplot(1,2,1)
    %plot(vecT,Aj(j,:)*1e6,'m+-')
    hold on
    %plot(vecT,Ac(j,:)*1e6,'r+-')
    plot(vecT,A(j,:)*1e6,['b' vecM(j) '-'])
    title('Farquhar model','FontSize',24)
    ylabel('CO2 assimilation rate (umol/m2/s)','FontSize',24)
    xlabel('T(°C)','FontSize',24)
    set(gca,'XLim',[5 60],'YLim',[0 3.5],'Fontsize',24)
    grid on

    
end



figure(1)
subplot(1,2,1)
legend(legend_graph,'Fontsize',24)
subplot(1,2,2)
legend(legend_graph,'Fontsize',24)








