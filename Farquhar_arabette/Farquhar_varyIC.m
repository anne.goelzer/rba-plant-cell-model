close all
clear all

indCO2 = 4; % C = 200ubar

% load result RBA
load Farquhar_RBA/RBA_varyIC.mat
nR = length(SolRBA);
nXX = length(SolRBA{indCO2}.sol.name_Xv2);
ind_Y_rubisco = max(strcmp('Y_R_RBC_h_enzyme',SolRBA{indCO2}.sol.name_Xv2).*[1:nXX]);
ind_Aass_CO2 = max(strcmp('nu_R_Im_CO2',SolRBA{indCO2}.sol.name_Xv2).*[1:nXX]);
ind_Rd_mito = max(strcmp('nu_R_Tr_CO2m',SolRBA{indCO2}.sol.name_Xv2).*[1:nXX]); % flux de CO2 excretes par la respiration
ind_Tp_chloro1 = max(strcmp('nu_R_Tr_X5P',SolRBA{indCO2}.sol.name_Xv2).*[1:nXX]); % flux de P imported in chloroplast
ind_Tp_chloro2 = max(strcmp('nu_R_Tr_GPT1',SolRBA{indCO2}.sol.name_Xv2).*[1:nXX]); % flux de P imported in chloroplast
ind_Tp_chloro3 = max(strcmp('nu_R_Tr_PPT',SolRBA{indCO2}.sol.name_Xv2).*[1:nXX]); % flux de P imported in chloroplast
ind_Tp_chloro4 = max(strcmp('nu_R_Tr_TPT3',SolRBA{indCO2}.sol.name_Xv2).*[1:nXX]); % flux de P imported in chloroplast
ind_Tp_chloro5 = max(strcmp('nu_R_Tr_TPT2',SolRBA{indCO2}.sol.name_Xv2).*[1:nXX]); % flux de P imported in chloroplast
ind_Tp_chloro6 = max(strcmp('nu_R_Tr_TPT1',SolRBA{indCO2}.sol.name_Xv2).*[1:nXX]); % flux de P imported in chloroplast
ind_Tp_chloro7 = max(strcmp('nu_R_Tr_NTT',SolRBA{indCO2}.sol.name_Xv2).*[1:nXX]); % flux de P imported in chloroplast
ind_Tp_chloro8 = max(strcmp('nu_R_ATPase_h',SolRBA{indCO2}.sol.name_Xv2).*[1:nXX]); % flux de P imported in chloroplast


% PhysioPlante.SLA -> specific leaf area in m2/gDW

conv_mM_day_in_uM_s = 1000/24/3600; 
conv_mM_day_in_mol_s = 1/1000/24/3600;
PhysioPlante.day_length = 8;
vecM = 'ox+*sdp';
gridIb = gridI/1e-3/PhysioPlante.SLA/3600/PhysioPlante.day_length;
A_rba = [];
for i=1:nR

    figure(1)
    subplot(1,2,2)
    plot(gridIb,SolRBA{i}.X(ind_Aass_CO2,:)/PhysioPlante.SLA*conv_mM_day_in_uM_s,['b' vecM(i) '-'])
    hold on
    title('RBA model','FontSize',24)
    ylabel('CO2 assimilation rate (umol/m2/s)','FontSize',24)
    xlabel('Irradiance (umol/m2/s)','FontSize',24)
    set(gca,'XLim',[0 2000],'FontSize',24)
    grid on
    
    A_rba = [A_rba; SolRBA{i}.X(ind_Aass_CO2,:)/PhysioPlante.SLA*conv_mM_day_in_uM_s];
end


% Farquhar simulation
MW_rbcL = 52955; % Da (=g/mol)
MW_rbcS = 20286; % Da (=g/mol)
MW_rubisco = MW_rbcL*8 + MW_rbcS*8;% car composés de 8 sous-unités chacune


%Fix O and T
O =210e-3; % =210e-3 partial pressure of O2, in bar
T = 25; %C
vecC = [55 110 165 200 230 330]*1e-6; nC = length(vecC);
vecI = [0:20:200 500:500:2000]*1e-6; nI = length(vecI); % irradiance mol of photos/m2/s
Ac = zeros(nC,nI);
Aj = zeros(nC,nI);
Ap = zeros(nC,nI);
A = zeros(nC,nI);
Rt = zeros(nC,nI);
Rd= zeros(nC,nI);
Tp= zeros(nC,nI);
legend_graph = {}
for j = 1:nC
    legend_graph = {legend_graph{:} [ num2str(vecC(j)/1e-6) ' ubar']};
    for i=1:nI
        I = vecI(i);
        C = vecC(j);

        % Rubisco content from RBA 
        Y_rubisco = SolRBA{j}.X(ind_Y_rubisco,i); % en mM/gDW
        % conversion en g/m2 = mM/gDW * g/mol * gDW/m2 
        Rt(j,i) = Y_rubisco*MW_rubisco/PhysioPlante.SLA/1000; 
        
        % Rd
        nu_Rd = abs(SolRBA{j}.X(ind_Rd_mito,i)); % mM/gDW/day
       
        % conversion in mol/m2/s
        Rd(j,i) = nu_Rd*conv_mM_day_in_mol_s/PhysioPlante.SLA;
        
         % Tp
        nu_Tp = abs(SolRBA{j}.X(ind_Tp_chloro1,i) ...
            - SolRBA{j}.X(ind_Tp_chloro2,i) ...
            - SolRBA{j}.X(ind_Tp_chloro3,i) ...
            + SolRBA{j}.X(ind_Tp_chloro4,i) ...
            + SolRBA{j}.X(ind_Tp_chloro5,i) ...
            + SolRBA{j}.X(ind_Tp_chloro6,i) ...
            - SolRBA{j}.X(ind_Tp_chloro7,i)); % mM/gDW/day

        % conversion en mol/m2/s
        Tp(j,i) = nu_Tp*conv_mM_day_in_mol_s/PhysioPlante.SLA;       
        [A(j,i),Ac(j,i),Aj(j,i),Ap(j,i)] = farquhar_full_Rd_free(T,Rt(j,i),Rd(j,i),Tp(j,i),I,O,C);
        
        
    end

    
    figure(1)
    subplot(1,2,1)
    %plot(vecT,Aj(j,:)*1e6,'m+-')
    hold on
    %plot(vecT,Ac(j,:)*1e6,'r+-')
    plot(vecI*1e6,A(j,:)*1e6,['b' vecM(j) '-'])
    title('Farquhar model','FontSize',24)
    ylabel('CO2 assimilation rate (umol/m2/s)','FontSize',24)
    xlabel('Irradiance (umol/m2/s)','FontSize',24)
    set(gca,'XLim',[0 2000],'YLim',[0 2.5],'Fontsize',24)
    grid on

    
end


figure(1)
subplot(1,2,1)
legend(legend_graph,'Fontsize',24)
subplot(1,2,2)
legend(legend_graph,'Fontsize',24)
