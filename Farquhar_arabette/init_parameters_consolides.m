
% MW rubisco
MW_rbcL = 52955; % Da (=g/mol)
MW_rbcS = 20286; % Da (=g/mol)
MW_rubisco = MW_rbcL*8 + MW_rbcS*8;% car composés de 8 sous-unités chacune

% rubisco parameter at 25°C
% Kc, Ko, Nsite actifs (0-8)
% From Table 5 of walker et al. Plant cell env, 2014.
Kc_25 = 312e-6; %bar 
E_Kc = 59.36e3; %J/mol
Ko_25 = 181e-3; % bar
E_Ko = 35.94e3; %J/mol


% From Table 5 of walker et al. Plant cell env, 2014.
ratio_Vomax_Vc_max = 0.24;
% turnover rate of 1 site: 3.5 1/s
% From Table 3 of walker et al. Plant cell env, 2014.
% in vitro kcat
kcat_c_max = 3.3; % determines comme  Vc_max_25/aR_25/Rt_rubisco_rba_25*MW_rubisco

E_Vcmax = 58.52e3; % J/mol

% Fraction of rubisco that is active
% T_sat_rubisco temperature for which rubisco is half-active
T_sat_rubisco = 310; % kelvin
aR = 1/(1+exp(0.3*(T_kelvin-T_sat_rubisco)));

% 
% Vc_max_25: From Table 4 of walker et al. Plant cell env, 2014.
Vc_max_25 = kcat_c_max*Rt/MW_rubisco; % mol/m2/s Vc_max 
Vo_max_25 = Vc_max_25*ratio_Vomax_Vc_max;
Sco_25 = Ko_25*Vc_max_25/Kc_25/Vo_max_25; % in bar/bar
Kc = Kc_25.*exp((T-25)*E_Kc./(298*R*(273+T))); % in bar
Ko = Ko_25.*exp((T-25)*E_Ko./(298*R*(273+T))); % in bar
Vc = aR*Vc_max_25.*exp((T-25)*E_Vcmax./(298*R*(273+T))); % in mol/m2/s
Vo = Vc*ratio_Vomax_Vc_max; % approximation
Sco = Ko*Vc/Kc/Vo;

% Jmax
% From Table 4 of walker et al. Plant cell env, 2014.
% ratio
ratio_Jmax25_Vcmax25 = 2.4441;
Jmax_leaf_25 = Vc_max_25*ratio_Jmax25_Vcmax25;

% light
light_absorbance = 0.85;
f = 0.15; % correction factor
H = 220e3; %J/mol
S = 710; % J/K/mol
E_Jmax = 37e3; % J/mol
Jmax = Jmax_leaf_25*exp((T_kelvin-298)*E_Jmax/(298*R*T_kelvin))*(1+exp((298*S-H)/298/R))/(1+exp((S*T_kelvin-H)/R/T_kelvin));

% theta
theta = 0.7; % empirical curve factor

